﻿using Api.Data.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Api.Data.Contexts
{
    public class AgileDataContext : DbContext
    {
        public AgileDataContext()
            : base(@"Data Source=www.runeclawgames.com;Initial Catalog=NewBasicAgileManager_Dev;Persist Security Info=True;User ID=Developer;Password=IMF04GTace")
        { }

        public AgileDataContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        { }

        public DbSet<Project> Projects { get; set; }

        public DbSet<Release> Releases { get; set; }

        public DbSet<Story> Stories { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<StoryStatus> StoryStatus { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}