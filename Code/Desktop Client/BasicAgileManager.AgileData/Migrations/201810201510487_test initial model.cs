namespace BasicAgileManager.AgileData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class testinitialmodel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Project",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OwnerId = c.Int(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        CreatedById = c.Int(nullable: false),
                        LastUpdatedById = c.Int(),
                        CreationDate = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                        LastUpdated = c.DateTime(),
                        User_Id = c.Int(),
                        User_Id1 = c.Int(),
                        User_Id2 = c.Int(),
                        Creater_Id = c.Int(),
                        Updater_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .ForeignKey("dbo.User", t => t.User_Id1)
                .ForeignKey("dbo.User", t => t.User_Id2)
                .ForeignKey("dbo.User", t => t.Creater_Id)
                .ForeignKey("dbo.User", t => t.OwnerId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.Updater_Id)
                .Index(t => t.OwnerId)
                .Index(t => t.User_Id)
                .Index(t => t.User_Id1)
                .Index(t => t.User_Id2)
                .Index(t => t.Creater_Id)
                .Index(t => t.Updater_Id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        SecureSalt = c.Binary(),
                        Name = c.String(),
                        AddressLineOne = c.String(),
                        AddressLineTwo = c.String(),
                        Town = c.String(),
                        Postcode = c.String(),
                        Telephone = c.String(),
                        Mobile = c.String(),
                        Email = c.String(),
                        Description = c.String(),
                        RoleId = c.Int(nullable: false),
                        Title = c.String(),
                        CreatedById = c.Int(nullable: false),
                        LastUpdatedById = c.Int(),
                        CreationDate = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                        LastUpdated = c.DateTime(),
                        Creater_Id = c.Int(),
                        Role_Id = c.Int(),
                        Updater_Id = c.Int(),
                        User_Id = c.Int(),
                        User_Id1 = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.Creater_Id)
                .ForeignKey("dbo.Role", t => t.Role_Id)
                .ForeignKey("dbo.Role", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.Updater_Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .ForeignKey("dbo.User", t => t.User_Id1)
                .Index(t => t.RoleId)
                .Index(t => t.Creater_Id)
                .Index(t => t.Role_Id)
                .Index(t => t.Updater_Id)
                .Index(t => t.User_Id)
                .Index(t => t.User_Id1);
            
            CreateTable(
                "dbo.Release",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OwnerId = c.Int(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        CreatedById = c.Int(nullable: false),
                        LastUpdatedById = c.Int(),
                        CreationDate = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                        LastUpdated = c.DateTime(),
                        Creater_Id = c.Int(),
                        Project_Id = c.Int(),
                        Updater_Id = c.Int(),
                        User_Id = c.Int(),
                        User_Id1 = c.Int(),
                        User_Id2 = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.Creater_Id)
                .ForeignKey("dbo.User", t => t.OwnerId, cascadeDelete: true)
                .ForeignKey("dbo.Project", t => t.Project_Id)
                .ForeignKey("dbo.User", t => t.Updater_Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .ForeignKey("dbo.User", t => t.User_Id1)
                .ForeignKey("dbo.User", t => t.User_Id2)
                .Index(t => t.OwnerId)
                .Index(t => t.Creater_Id)
                .Index(t => t.Project_Id)
                .Index(t => t.Updater_Id)
                .Index(t => t.User_Id)
                .Index(t => t.User_Id1)
                .Index(t => t.User_Id2);
            
            CreateTable(
                "dbo.Story",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReleaseId = c.Int(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        StatusId = c.Int(nullable: false),
                        OwnerId = c.Int(),
                        ChangeSet = c.Long(),
                        TFSID = c.Long(),
                        Priority = c.Int(nullable: false),
                        CreatedById = c.Int(nullable: false),
                        LastUpdatedById = c.Int(),
                        CreationDate = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                        LastUpdated = c.DateTime(),
                        Creater_Id = c.Int(),
                        Updater_Id = c.Int(),
                        User_Id = c.Int(),
                        User_Id1 = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.Creater_Id)
                .ForeignKey("dbo.Release", t => t.ReleaseId, cascadeDelete: true)
                .ForeignKey("dbo.StoryStatus", t => t.StatusId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.Updater_Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .ForeignKey("dbo.User", t => t.User_Id1)
                .Index(t => t.ReleaseId)
                .Index(t => t.StatusId)
                .Index(t => t.Creater_Id)
                .Index(t => t.Updater_Id)
                .Index(t => t.User_Id)
                .Index(t => t.User_Id1);
            
            CreateTable(
                "dbo.StoryStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        CreatedById = c.Int(nullable: false),
                        LastUpdatedById = c.Int(),
                        CreationDate = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                        LastUpdated = c.DateTime(),
                        Creater_Id = c.Int(),
                        Updater_Id = c.Int(),
                        User_Id = c.Int(),
                        User_Id1 = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.Creater_Id)
                .ForeignKey("dbo.User", t => t.Updater_Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .ForeignKey("dbo.User", t => t.User_Id1)
                .Index(t => t.Creater_Id)
                .Index(t => t.Updater_Id)
                .Index(t => t.User_Id)
                .Index(t => t.User_Id1);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleTitle = c.String(),
                        Description = c.String(),
                        CreatedById = c.Int(nullable: false),
                        LastUpdatedById = c.Int(),
                        CreationDate = c.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"),
                        LastUpdated = c.DateTime(),
                        Creater_Id = c.Int(),
                        Updater_Id = c.Int(),
                        User_Id = c.Int(),
                        User_Id1 = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.Creater_Id)
                .ForeignKey("dbo.User", t => t.Updater_Id)
                .ForeignKey("dbo.User", t => t.User_Id)
                .ForeignKey("dbo.User", t => t.User_Id1)
                .Index(t => t.Creater_Id)
                .Index(t => t.Updater_Id)
                .Index(t => t.User_Id)
                .Index(t => t.User_Id1);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Project", "Updater_Id", "dbo.User");
            DropForeignKey("dbo.Project", "OwnerId", "dbo.User");
            DropForeignKey("dbo.Project", "Creater_Id", "dbo.User");
            DropForeignKey("dbo.User", "User_Id1", "dbo.User");
            DropForeignKey("dbo.User", "User_Id", "dbo.User");
            DropForeignKey("dbo.User", "Updater_Id", "dbo.User");
            DropForeignKey("dbo.Story", "User_Id1", "dbo.User");
            DropForeignKey("dbo.Story", "User_Id", "dbo.User");
            DropForeignKey("dbo.StoryStatus", "User_Id1", "dbo.User");
            DropForeignKey("dbo.StoryStatus", "User_Id", "dbo.User");
            DropForeignKey("dbo.Role", "User_Id1", "dbo.User");
            DropForeignKey("dbo.Role", "User_Id", "dbo.User");
            DropForeignKey("dbo.User", "RoleId", "dbo.Role");
            DropForeignKey("dbo.User", "Role_Id", "dbo.Role");
            DropForeignKey("dbo.Role", "Updater_Id", "dbo.User");
            DropForeignKey("dbo.Role", "Creater_Id", "dbo.User");
            DropForeignKey("dbo.Release", "User_Id2", "dbo.User");
            DropForeignKey("dbo.Release", "User_Id1", "dbo.User");
            DropForeignKey("dbo.Release", "User_Id", "dbo.User");
            DropForeignKey("dbo.Release", "Updater_Id", "dbo.User");
            DropForeignKey("dbo.Story", "Updater_Id", "dbo.User");
            DropForeignKey("dbo.StoryStatus", "Updater_Id", "dbo.User");
            DropForeignKey("dbo.Story", "StatusId", "dbo.StoryStatus");
            DropForeignKey("dbo.StoryStatus", "Creater_Id", "dbo.User");
            DropForeignKey("dbo.Story", "ReleaseId", "dbo.Release");
            DropForeignKey("dbo.Story", "Creater_Id", "dbo.User");
            DropForeignKey("dbo.Release", "Project_Id", "dbo.Project");
            DropForeignKey("dbo.Release", "OwnerId", "dbo.User");
            DropForeignKey("dbo.Release", "Creater_Id", "dbo.User");
            DropForeignKey("dbo.Project", "User_Id2", "dbo.User");
            DropForeignKey("dbo.Project", "User_Id1", "dbo.User");
            DropForeignKey("dbo.Project", "User_Id", "dbo.User");
            DropForeignKey("dbo.User", "Creater_Id", "dbo.User");
            DropIndex("dbo.Role", new[] { "User_Id1" });
            DropIndex("dbo.Role", new[] { "User_Id" });
            DropIndex("dbo.Role", new[] { "Updater_Id" });
            DropIndex("dbo.Role", new[] { "Creater_Id" });
            DropIndex("dbo.StoryStatus", new[] { "User_Id1" });
            DropIndex("dbo.StoryStatus", new[] { "User_Id" });
            DropIndex("dbo.StoryStatus", new[] { "Updater_Id" });
            DropIndex("dbo.StoryStatus", new[] { "Creater_Id" });
            DropIndex("dbo.Story", new[] { "User_Id1" });
            DropIndex("dbo.Story", new[] { "User_Id" });
            DropIndex("dbo.Story", new[] { "Updater_Id" });
            DropIndex("dbo.Story", new[] { "Creater_Id" });
            DropIndex("dbo.Story", new[] { "StatusId" });
            DropIndex("dbo.Story", new[] { "ReleaseId" });
            DropIndex("dbo.Release", new[] { "User_Id2" });
            DropIndex("dbo.Release", new[] { "User_Id1" });
            DropIndex("dbo.Release", new[] { "User_Id" });
            DropIndex("dbo.Release", new[] { "Updater_Id" });
            DropIndex("dbo.Release", new[] { "Project_Id" });
            DropIndex("dbo.Release", new[] { "Creater_Id" });
            DropIndex("dbo.Release", new[] { "OwnerId" });
            DropIndex("dbo.User", new[] { "User_Id1" });
            DropIndex("dbo.User", new[] { "User_Id" });
            DropIndex("dbo.User", new[] { "Updater_Id" });
            DropIndex("dbo.User", new[] { "Role_Id" });
            DropIndex("dbo.User", new[] { "Creater_Id" });
            DropIndex("dbo.User", new[] { "RoleId" });
            DropIndex("dbo.Project", new[] { "Updater_Id" });
            DropIndex("dbo.Project", new[] { "Creater_Id" });
            DropIndex("dbo.Project", new[] { "User_Id2" });
            DropIndex("dbo.Project", new[] { "User_Id1" });
            DropIndex("dbo.Project", new[] { "User_Id" });
            DropIndex("dbo.Project", new[] { "OwnerId" });
            DropTable("dbo.Role");
            DropTable("dbo.StoryStatus");
            DropTable("dbo.Story");
            DropTable("dbo.Release");
            DropTable("dbo.User");
            DropTable("dbo.Project");
        }
    }
}
