namespace BasicAgileManager.AgileData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class futurepatch : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Story", "OwnerId");
            AddForeignKey("dbo.Story", "OwnerId", "dbo.User", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Story", "OwnerId", "dbo.User");
            DropIndex("dbo.Story", new[] { "OwnerId" });
        }
    }
}
