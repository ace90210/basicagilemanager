namespace BasicAgileManager.AgileData.Migrations
{
    using Api.Data.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Model;
    using System.Data.Entity.SqlServer;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Api.Data.Contexts.AgileDataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Api.Data.Contexts.AgileDataContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            var roles = new List<Role>()
            {
                new Role()
                {
                    RoleTitle = "Admin"
                }
            };

            var users = new List<User>()
            {
                new User()
                {
                    Username = "Admin",
                    Password = "matrix2004",
                    Role = roles[0]
                }
            };

            context.Roles.AddOrUpdate(roles.ToArray());
            context.SaveChanges();

            context.Users.AddOrUpdate(users.ToArray());
            context.SaveChanges();
            
            roles[0].Creater = users[0];
            context.SaveChanges();
        }
    }
    

    internal class CustomSqlServerMigrationSqlGenerator : SqlServerMigrationSqlGenerator
    {
        protected override void Generate(AddColumnOperation addColumnOperation)
        {
            SetCreationDateColumn(addColumnOperation.Column);

            base.Generate(addColumnOperation);
        }

        protected override void Generate(CreateTableOperation createTableOperation)
        {
            SetCreationDateColumn(createTableOperation.Columns);

            base.Generate(createTableOperation);
        }

        private static void SetCreationDateColumn(IEnumerable<ColumnModel> columns)
        {
            foreach (var columnModel in columns)
            {
                SetCreationDateColumn(columnModel);
            }
        }

        private static void SetCreationDateColumn(PropertyModel column)
        {
            if (column.Name == "CreationDate")
            {
                column.DefaultValueSql = "GETUTCDATE()";
            }
        }
    }
}
