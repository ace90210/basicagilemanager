﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Api.Data.Models
{
    public class AuditedTable
    {
        public int CreatedById { get; set; }
        public Nullable<int> LastUpdatedById { get; set; }

        public virtual User Creater { get; set; }
        public virtual User Updater { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public System.DateTime CreationDate { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
    }
}
