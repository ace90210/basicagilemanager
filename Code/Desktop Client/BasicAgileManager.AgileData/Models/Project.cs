namespace Api.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public class Project : AuditedTable
    {    
        public int Id { get; set; }
        public int OwnerId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Release> Releases { get; set; }
        public virtual User Owner { get; set; }
    }
}
