namespace Api.Data.Models
{
    using System.Collections.Generic;

    public partial class Release : AuditedTable
    {    
        public int Id { get; set; }
        public int OwnerId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    
        public virtual Project Project { get; set; }
        public virtual ICollection<Story> Stories { get; set; }
        public virtual User Owner { get; set; }
    }
}
