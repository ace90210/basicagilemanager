namespace Api.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Story : AuditedTable
    {
        public int Id { get; set; }
        public int ReleaseId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int StatusId { get; set; }
        public Nullable<int> OwnerId { get; set; }
        public Nullable<long> ChangeSet { get; set; }
        public Nullable<long> TFSID { get; set; }
        public int Priority { get; set; }


        public virtual User Owner { get; set; }
        public virtual Release Release { get; set; }
        public virtual StoryStatus Status { get; set; }
    }
}
