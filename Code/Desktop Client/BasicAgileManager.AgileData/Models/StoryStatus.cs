namespace Api.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class StoryStatus : AuditedTable
    {    
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    
        public virtual ICollection<Story> Stories { get; set; }
    }
}
