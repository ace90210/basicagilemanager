namespace Api.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class User : AuditedTable
    {    
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public byte[] SecureSalt { get; set; }
        public string Name { get; set; }
        public string AddressLineOne { get; set; }
        public string AddressLineTwo { get; set; }
        public string Town { get; set; }
        public string Postcode { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public int RoleId { get; set; }
        public string Title { get; set; }

        public virtual ICollection<Project> ProjectsCreated { get; set; }
        public virtual ICollection<Project> ProjectsRecentUpdated { get; set; }
        public virtual ICollection<Release> ReleasesCreated { get; set; }
        public virtual ICollection<Release> ReleasesRecentlyUpdated { get; set; }
        public virtual ICollection<Role> RolesCreated { get; set; }
        public virtual ICollection<Role> RolesRecentlyUpdated { get; set; }
        public virtual Role Role { get; set; }
        public virtual ICollection<Story> StoriesCreated { get; set; }
        public virtual ICollection<Story> StoriesRecentlyUpdated { get; set; }
        public virtual ICollection<StoryStatus> StatusesCreated { get; set; }
        public virtual ICollection<StoryStatus> StatusesRecentlyUpdated { get; set; }
        public virtual ICollection<User> UsersCreated { get; set; }
        public virtual ICollection<User> UsersRecentlyUpdated { get; set; }
        public virtual ICollection<Release> Releases { get; set; }
        public virtual ICollection<Project> Projects { get; set; }
    }
}
