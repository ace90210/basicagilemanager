﻿using EnterpriseMVVMTemplate.DesktopClient.Views;
using System.Windows;

namespace EnterpriseMVVMTemplate.DesktopClient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            //MainWindow window = new MainWindow ( new MainWindowViewModel());

            Login window = new Login();

            window.ShowDialog();
        }    
    }
}
