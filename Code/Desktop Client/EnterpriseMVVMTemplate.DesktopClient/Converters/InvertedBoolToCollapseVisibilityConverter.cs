﻿using System.Windows;
using System.Windows.Data;

namespace EnterpriseMVVMTemplate.DesktopClient.Converters
{
    public class InvertedBoolToCollapseVisibilityConverter : IValueConverter
    {

        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is bool))
            {
                return Visibility.Visible;
            }

            if ((bool)value)
            {
                return Visibility.Collapsed;
            }
            return Visibility.Visible;
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }
    }
}