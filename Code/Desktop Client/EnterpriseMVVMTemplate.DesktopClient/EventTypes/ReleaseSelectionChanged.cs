﻿using EnterpriseMVVMTemplate.Windows;
using Microsoft.Practices.Prism.PubSubEvents;

namespace EnterpriseMVVMTemplate.DesktopClient.EventTypes
{
    public class ReleaseSelectionChanged: PubSubEvent<ViewModelBase>
    {
    }
}
