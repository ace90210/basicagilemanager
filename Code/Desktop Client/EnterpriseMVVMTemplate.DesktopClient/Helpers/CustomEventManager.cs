﻿using Microsoft.Practices.Prism.PubSubEvents;

namespace EnterpriseMVVMTemplate.DesktopClient.Helpers
{
    public static class CustomEventManager
    {
        private static IEventAggregator _eventAggregator;

        public static IEventAggregator Instance
        {
            get
            {
                if (_eventAggregator == null)
                {
                    _eventAggregator = new EventAggregator();
                }
                return _eventAggregator;
            }
        }
    }
}
