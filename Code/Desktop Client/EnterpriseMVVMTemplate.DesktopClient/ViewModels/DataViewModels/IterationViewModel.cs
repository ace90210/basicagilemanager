﻿using System;
using EnterpriseMVVMTemplate.Windows;
using Api.Data.Contexts; using Api.Data.Models;
using BasicAgileManager.AgileData.Models;

namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels.DataViewModels
{
    public class IterationViewModel : ViewModelBase
    {
        private readonly Iteration _iteration;

        public IterationViewModel(Iteration iteration)
        {
            _iteration = iteration;
        }

        public int Id
        {
            get { return _iteration.Id; }
        }

        public string Title
        {
            get { return _iteration.Title; }
            set
            {
                _iteration.Title = value;
                OnPropertyChanged();
            }
        }

        public DateTime StartDate
        {
            get { return _iteration.StartDate; }
            set
            {
                _iteration.StartDate = value;
                OnPropertyChanged();
            }
        }

        public DateTime EndDate
        {
            get { return _iteration.EndDate; }
            set
            {
                _iteration.EndDate = value;
                OnPropertyChanged();
            }
        }

        public Iteration Iteration
        {
            get
            {
                return _iteration;
            }
        }

        public int ReleaseId
        {
            get { return _iteration.ReleaseId; }
            set
            {
                _iteration.ReleaseId = value;
                OnPropertyChanged();
            }
        }

        public Release Release
        {
            get { return _iteration.Release; }
            set
            {
                _iteration.Release = value;
                OnPropertyChanged();
            }
        }

        public DateTime CreatedDate
        {
            get { return _iteration.CreationDate; }
            set { _iteration.CreationDate = value; }
        }

        public DateTime? UpdatedDate
        {
            get { return _iteration.LastUpdated; }
            set
            {
                _iteration.LastUpdated = value;
                OnPropertyChanged();
            }
        }

        public int CreatedById
        {
            get { return _iteration.CreatedById; }
            set
            {
                _iteration.CreatedById = value;
                OnPropertyChanged();
            }
        }

        public User CreatedBy
        {
            get { return _iteration.Creater; }
            set
            {
                _iteration.Creater = value;
                OnPropertyChanged();
            }
        }

        public int? UpdatedById
        {
            get
            {
                return _iteration.LastUpdatedById;
            }
            set
            {
                _iteration.LastUpdatedById = value;
                OnPropertyChanged();
            }
        }

        public User UpdatedBy
        {
            get { return _iteration.Updater; }
            set
            {
                _iteration.Updater = value;
                OnPropertyChanged();
            }
        }
    }
}
