﻿using System;
using System.Collections.ObjectModel;
using EnterpriseMVVMTemplate.Windows;
using Api.Data.Contexts; using Api.Data.Models;

namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels.DataViewModels
{
    public class ProjectViewModel : ViewModelBase
    {
        public static Project NullProject = new Project { Id = -1, Title = Strings.NotSpecified };
        public static ProjectViewModel SelectedProjectViewModel;

        private Project _project;

        public ProjectViewModel(Project project)
        {
            _project = project;
        }

        public int Id
        {
            get { return _project.Id; }
        }

        public string Title
        {
            get { return _project.Title; }
            set
            {
                _project.Title = value;
                OnPropertyChanged();
            }
        }

        public string Description
        {
            get { return _project.Description; }
            set
            {
                _project.Description = value;
                OnPropertyChanged();
            }
        }

        public Project Project
        {
            get
            {
                return _project;
            }
        }

        public ObservableCollection<Release> Releases
        {
            get { return new ObservableCollection<Release>(_project.Releases); }
            set
            {
                _project.Releases = value;
                OnPropertyChanged();
            }
        }

        public User Owner
        {
            get { return _project.Owner; }
            set
            {
                _project.Owner = value;
                OnPropertyChanged();
            }
        }

        public DateTime CreatedDate
        {
            get { return _project.CreationDate; }
        }

        public DateTime? UpdatedDate
        {
            get { return _project.LastUpdated; }
            set
            {
                _project.LastUpdated = value;
                OnPropertyChanged();
            }
        }

        public User CreatedBy
        {
            get { return _project.Creater; }
            set
            {
                _project.Creater = value;
                OnPropertyChanged();
            }
        }

        public User UpdatedBy
        {
            get { return _project.Updater; }
            set
            {
                _project.Updater = value;
                OnPropertyChanged();
            }
        }
    }
}
