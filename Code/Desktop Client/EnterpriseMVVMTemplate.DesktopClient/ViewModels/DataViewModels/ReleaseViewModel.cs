﻿using System;
using EnterpriseMVVMTemplate.Windows;
using Api.Data.Contexts; using Api.Data.Models;

namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels.DataViewModels
{
    public class ReleaseViewModel : ViewModelBase
    {
        public static ReleaseViewModel SelectedModelViewModel;
        public static readonly Release NullRelease = new Release { Id = -1, Title = Strings.NotSpecified };

        private readonly Release _release;

        public ReleaseViewModel(Release release)
        {
            _release = release;
        }

        public int Id
        {
            get { return _release.Id; }
        }

        public string Title
        {
            get { return _release.Title; }
            set
            {
                _release.Title = value;
                OnPropertyChanged();
            }
        }

        public string Description
        {
            get { return _release.Description; }
            set
            {
                _release.Description = value;
                OnPropertyChanged();
            }
        }

        public Project Project
        {
            get { return _release.Project; }
            set
            {
                _release.Project = value;
                OnPropertyChanged();
            }
        }

        public User Owner
        {
            get { return _release.Owner; }
            set
            {
                _release.Owner = value;
                OnPropertyChanged();
            }
        }

        public DateTime CreatedDate
        {
            get { return _release.CreationDate; }
        }

        public DateTime? UpdatedDate
        {
            get { return _release.LastUpdated; }
            set
            {
                _release.LastUpdated = value;
                OnPropertyChanged();
            }
        }

        public User CreatedBy
        {
            get { return _release.Creater; }
            set
            {
                _release.Creater = value;
                OnPropertyChanged();
            }
        }

        public User UpdatedBy
        {
            get { return _release.Updater; }
            set
            {
                _release.Updater = value;
                OnPropertyChanged();
            }
        }

        public Release Release
        {
            get { return _release; }
        }
    }
}
