﻿using System;
using EnterpriseMVVMTemplate.Windows;
using Api.Data.Contexts; using Api.Data.Models;
using BasicAgileManager.AgileData.Models;

namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels.DataViewModels
{
    public class StoryViewModel : ViewModelBase
    {
        private Story _story;

        public StoryViewModel(Story story)
        {
            _story = story;
        }

        public int Id
        {
            get { return _story.Id; }
        }

        public string Title
        {
            get { return _story.Title; }
            set
            {
                _story.Title = value;
                OnPropertyChanged();
            }
        }

        public string Description
        {
            get { return _story.Description; }
            set
            {
                _story.Description = value;
                OnPropertyChanged();
            }
        }

        public Release TargetRelease
        {
            get { return _story.Release; }
            set
            {
                _story.Release = value;
                OnPropertyChanged();
            }
        }

        public User Owner
        {
            get { return _story.Owner; }
            set
            {
                _story.Owner = value;
                OnPropertyChanged();
            }
        }

        public DateTime CreatedDate
        {
            get { return _story.CreationDate; }
        }

        public DateTime? UpdatedDate
        {
            get { return _story.LastUpdated; }
            set
            {
                _story.LastUpdated = value;
                OnPropertyChanged();
            }
        }

        public User CreatedBy
        {
            get { return _story.Creater; }
            set
            {
                _story.Creater = value;
                OnPropertyChanged();
            }
        }

        public User UpdatedBy
        {
            get { return _story.Updater; }
            set
            {
                _story.Updater = value;
                OnPropertyChanged();
            }
        }

        public Iteration Iteration
        {
            get
            {
                return _story.Iteration;
            }
            set
            {
                _story.Iteration = value;
                if (value != null)
                {
                    _story.IterationId = value.Id;
                }
                OnPropertyChanged();
            }
        }

    }
}
