﻿using System.Collections.ObjectModel;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels.DataViewModels;
using EnterpriseMVVMTemplate.Windows;
using EnterpriseMVVMTemplate.Windows.ValidationRules;
using System;
using System.Linq;
using Api.Data.Contexts; using Api.Data.Models;
using BasicAgileManager.AgileData.Models;

namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels
{
    public class EditIterationViewModel : ViewModelBase
    {
        #region Fields
        private ReleaseViewModel _selectedRelease;
        private IterationViewModel _iteration;
        #endregion

        public EditIterationViewModel(IterationViewModel iteration, ObservableCollection<ReleaseViewModel> releases)
        {
            if (iteration == null)
            {
                IsNew = true;
                _iteration = new IterationViewModel(new Iteration());
                SelectedRelease = releases.FirstOrDefault();

                if (SelectedRelease != null &&
                    SelectedRelease.Release != null &&
                    SelectedRelease.Release.Iterations != null &&
                    SelectedRelease.Release.Iterations.Count > 0)
                {
                    _iteration.StartDate = SelectedRelease.Release.Iterations.Max(it => it.EndDate).AddDays(1);
                }
                else
                {
                    _iteration.StartDate = DateTime.Now;
                }
                _iteration.EndDate = _iteration.StartDate.AddDays(13);
            }
            else
            {
                IsNew = false;
                _iteration = iteration;

                User createdUser = _iteration.CreatedBy;
                CreatedBy = createdUser == null ? "Not Found" : createdUser.Username;

                if (_iteration.UpdatedBy != null)
                {
                    User updatedUser = _iteration.UpdatedBy;

                    UpdatedBy = updatedUser == null ? "Not Found" : updatedUser.Username;
                }
                SelectedRelease = releases.FirstOrDefault(r => r.Release == _iteration.Release);
            }

            Releases = releases;
            
            //register validation rules
            //name
            ValidationRules.Add(new ValidationStringRule("Title", "Title")
            {
                MinLength = 1,
                MaxLength = 32
            });

            //parent project
            ValidationRules.Add(new ValidationRule("Parent Release", "SelectedRelease", ValidateParent) { UseCustomDelegateOnly = true }); 

            SaveChangesCommand = new ActionCommand(SaveChanges, param => CanSave());
        }

        private string ValidateParent()
        {
            const string errorDesc = "A Release must be selected";
            Errors.Remove(errorDesc);
            if (SelectedRelease == null ||
                SelectedRelease.Release == null ||
                SelectedRelease.Release.Id == 0)
            {
                Errors.Add(errorDesc);
                return errorDesc;
            }
            return null;
        }
        
        #region Commands
        public ActionCommand SaveChangesCommand
        {
            get;
            private set;
        }

        public bool CanSave()
        {
            if (HasErrors || Errors.Any())
            {
                return false;
            }
            return true;
        }

        public void SaveChanges(object obj = null)
        {

            if (IsNew)
            {
                _iteration.CreatedById = LoggedInUser.Id;
                _iteration.CreatedDate = DateTime.Now;
                //add release
                Manager.AddIteration(_iteration.Iteration);
                IsNew = false;
            }
            else
            {
                //update models last updated fields
                _iteration.UpdatedById = LoggedInUser.Id;
                _iteration.UpdatedDate = DateTime.Now;
                //update release
                Manager.UpdateIteration(_iteration.Iteration);
            }

            Status = "Saved";
            OnPropertyChanged("Status");
            Close(null);
        }
        #endregion

        #region Validation

        protected override string OnValidate(string propertyName)
        {
            string error = base.OnValidate(propertyName);

            OnPropertyChanged("Status");

            return error;
        }

        #endregion

        #region Properties

        public bool IsNew { get; set; }

        private string _status;
        public string Status
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_status) && Errors.Any())
                {
                    return Errors.FirstOrDefault();
                }
                return _status;

            }
            private set
            {
                _status = value;
                OnPropertyChanged();
            }
        }

        public string Title
        {
            get
            {
                return _iteration.Title;
            }
            set
            {
                if (_iteration != null)
                {
                    _iteration.Title = value;
                }
                OnPropertyChanged();
            }
        }

        public DateTime StartDate
        {
            get
            {
                return _iteration.StartDate;
            }
            set
            {
                if (_iteration != null)
                {
                    _iteration.StartDate = value;
                }
                OnPropertyChanged();
            }
        }

        public DateTime EndDate
        {
            get
            {
                return _iteration.EndDate;
            }
            set
            {
                if (_iteration != null)
                {
                    _iteration.EndDate = value;
                }
                OnPropertyChanged();
            }
        }

        public Iteration Iteration
        {
            get { return _iteration.Iteration; }
        }

        public string CreatedBy { get; private set; }
        public string UpdatedBy { get; private set; }

        public DateTime? CreatedDate
        {
            get
            {
                return _iteration.CreatedDate;
            }
        }
        public DateTime? UpdatedDate
        {
            get
            {
                if (_iteration.UpdatedDate != null)
                    return _iteration.UpdatedDate.Value;
                return null;
            }
        }

        public ObservableCollection<ReleaseViewModel> Releases
        {
            get;
            set;
        }

        public ReleaseViewModel SelectedRelease
        {
            get
            {
                return _selectedRelease;
            }
            set
            {
                _selectedRelease = value;
                OnPropertyChanged();
                if (_selectedRelease != null)
                {
                    _iteration.ReleaseId = _selectedRelease.Id;
                }
            }
        }
        #endregion
    }
}