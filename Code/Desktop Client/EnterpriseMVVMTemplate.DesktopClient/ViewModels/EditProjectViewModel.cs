﻿using EnterpriseMVVMTemplate.Windows;
using EnterpriseMVVMTemplate.Windows.ValidationRules;
using System;
using System.Linq;
using Api.Data.Contexts; using Api.Data.Models;

namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels
{
    public class EditProjectViewModel : ViewModelBase
    {
        #region Fields
        
        private Project _project;
        #endregion

        public EditProjectViewModel(Project project)
        {
            if (project == null)
            {
                IsNew = true;
                _project = new Project();
                _project.OwnerId = LoggedInUser.Id;
            }
            else
            {
                IsNew = false;
                _project = project;

                User createdUser = Manager.GetUserById(_project.CreatedById);
                CreatedBy = createdUser == null ? "Not Found" : createdUser.Username;

                if (_project.LastUpdatedById != null)
                {
                    User updatedUser = Manager.GetUserById((int)_project.LastUpdatedById);

                    UpdatedBy = updatedUser == null ? "Not Found" : updatedUser.Username;
                }
            }

            //register validation rules
            //name
            ValidationRules.Add(new ValidationStringRule("Title", "Title")
            {
                MinLength = 4,
                MaxLength = 64
            });

            SaveChangesCommand = new ActionCommand(SaveChanges, param => CanSave());
        }
        
        #region Commands
        public ActionCommand SaveChangesCommand
        {
            get;
            private set;
        }

        public bool CanSave()
        {
            if (HasErrors || Errors.Any())
            {
                return false;
            }
            return true;
        }

        public void SaveChanges(object obj = null)
        {
            if (IsNew)
            {
                _project.CreatedById = LoggedInUser.Id;
                _project.CreationDate = DateTime.Now;
                //add project
                Manager.AddProject(_project);
                IsNew = false;
            }
            else
            {

                //update models last updated fields
                _project.LastUpdatedById = LoggedInUser.Id;
                _project.LastUpdated = DateTime.Now;

                //update project
                Manager.UpdateProject(_project);
            }

            Status = "Saved";
            OnPropertyChanged("Status");
            Close(null);
        }
        #endregion

        #region Validation

        protected override string OnValidate(string propertyName)
        {
            string error = base.OnValidate(propertyName);

            OnPropertyChanged("Status");

            return error;
        }

        #endregion

        #region Properties

        public bool IsNew { get; set; }

        private string _status;

        public string Status
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_status) && Errors.Any())
                {
                    return Errors.FirstOrDefault();
                }
                return _status;

            }
            private set
            {
                _status = value;
                OnPropertyChanged();
            }
        }

        public string Title
        {
            get
            {
                return _project.Title;
            }
            set
            {
                if (_project != null)
                {
                    _project.Title = value;
                }
                OnPropertyChanged();
            }
        }

        public string Description
        {
            get
            {
                return _project.Description;
            }
            set
            {
                if (_project != null)
                {
                    _project.Description = value;
                }
                OnPropertyChanged();
            }
        }

        public int OwnerId
        {
            get { return _project.OwnerId; }
            set { _project.OwnerId = value; }
        }

        public Project Project
        {
            get { return _project; }
        }

        public string CreatedBy { get; private set; }
        public string UpdatedBy { get; private set; }

        public DateTime? CreatedDate
        {
            get
            {
                return _project.CreationDate;
            }
        }
        public DateTime? UpdatedDate
        {
            get
            {
                if (_project.LastUpdated != null)
                    return _project.LastUpdated.Value;
                return null;
            }
        }
        #endregion
    }
}