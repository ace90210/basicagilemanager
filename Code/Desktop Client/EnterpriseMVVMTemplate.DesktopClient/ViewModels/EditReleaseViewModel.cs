﻿using System.Collections.ObjectModel;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels.DataViewModels;
using EnterpriseMVVMTemplate.Windows;
using EnterpriseMVVMTemplate.Windows.ValidationRules;
using System;
using System.Linq;
using Api.Data.Contexts; using Api.Data.Models;

namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels
{
    public class EditReleaseViewModel : ViewModelBase
    {

        #region Fields
        private readonly Release _release;
        private ProjectViewModel _selectedParentProject;
        #endregion

        public EditReleaseViewModel(Release release, ObservableCollection<ProjectViewModel> projects)
        {
            this.Projects = new ObservableCollection<ProjectViewModel>(Manager.GetAllProjects().Select(p => new ProjectViewModel(p)));
            if (release == null)
            {
                IsNew = true;
                _release = new Release();
                _release.OwnerId = LoggedInUser.Id;
                SelectedParentProject = projects.FirstOrDefault();
            }
            else
            {
                IsNew = false;
                _release = release;

                User createdUser = Manager.GetUserById(_release.CreatedById);
                CreatedBy = createdUser == null ? "Not Found" : createdUser.Username;

                if (_release.LastUpdatedById != null)
                {
                    User updatedUser = Manager.GetUserById((int)_release.LastUpdatedById);

                    UpdatedBy = updatedUser == null ? "Not Found" : updatedUser.Username;
                }
                SelectedParentProject = projects.FirstOrDefault(p => p.Project == _release.Project);
            }

            OnPropertyChanged("Projects");

            //register validation rules
            //name
            ValidationRules.Add(new ValidationStringRule("Title", "Title")
            {
                MinLength = 1,
                MaxLength = 64
            });

            //parent project
            ValidationRules.Add(new ValidationRule("Parent Project", "SelectedParentProject", ValidateParent) { UseCustomDelegateOnly = true }); 

            SaveChangesCommand = new ActionCommand(SaveChanges, param => CanSave());
        }

        private string ValidateParent()
        {
            const string errorDesc = "A Project must be selected";
            Errors.Remove(errorDesc);
            if (SelectedParentProject == null ||
                SelectedParentProject.Project == null ||
                SelectedParentProject.Project.Id == 0)
            {
                Errors.Add(errorDesc);
                return errorDesc;
            }
            return null;
        }
        
        #region Commands
        public ActionCommand SaveChangesCommand
        {
            get;
            private set;
        }

        public bool CanSave()
        {
            if (HasErrors || Errors.Any())
            {
                return false;
            }
            return true;
        }

        public void SaveChanges(object obj = null)
        {

            if (IsNew)
            {
                _release.CreatedById = LoggedInUser.Id;
                _release.CreationDate = DateTime.Now;
                //add release
                Manager.AddRelease(_release);
                IsNew = false;
            }
            else
            {
                //update models last updated fields
                _release.LastUpdatedById = LoggedInUser.Id;
                _release.LastUpdated = DateTime.Now;
                //update release
                Manager.UpdateRelease(_release);
            }

            Status = "Saved";
            OnPropertyChanged("Status");
            Close(null);
        }
        #endregion

        #region Validation

        protected override string OnValidate(string propertyName)
        {
            string error = base.OnValidate(propertyName);

            OnPropertyChanged("Status");

            return error;
        }

        #endregion

        #region Properties

        public bool IsNew { get; set; }

        private string _status;

        public string Status
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_status) && Errors.Any())
                {
                    return Errors.FirstOrDefault();
                }
                return _status;

            }
            private set
            {
                _status = value;
                OnPropertyChanged();
            }
        }

        public string Title
        {
            get
            {
                return _release.Title;
            }
            set
            {
                if (_release != null)
                {
                    _release.Title = value;
                }
                OnPropertyChanged();
            }
        }

        public string Description
        {
            get
            {
                return _release.Description;
            }
            set
            {
                if (_release != null)
                {
                    _release.Description = value;
                }
                OnPropertyChanged();
            }
        }

        public int OwnerId
        {
            get { return _release.OwnerId; }
            set { _release.OwnerId = value; }
        }

        public Release Release
        {
            get { return _release; }
        }

        public string CreatedBy { get; private set; }
        public string UpdatedBy { get; private set; }

        public DateTime? CreatedDate
        {
            get
            {
                return _release.CreationDate;
            }
        }
        public DateTime? UpdatedDate
        {
            get
            {
                if (_release.LastUpdated != null)
                    return _release.LastUpdated.Value;
                return null;
            }
        }

        public ObservableCollection<ProjectViewModel> Projects
        {
            get;
            set;
        }

        public ProjectViewModel SelectedParentProject
        {
            get
            {
                return _selectedParentProject;
            }
            set
            {
                _selectedParentProject = value;
                OnPropertyChanged();
                if (_selectedParentProject != null)
                {
                    Release.Project = _selectedParentProject.Project;
                }
            }
        }
        #endregion
    }
}