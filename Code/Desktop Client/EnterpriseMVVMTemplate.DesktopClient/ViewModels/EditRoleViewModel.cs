﻿using System.Linq;
using EnterpriseMVVMTemplate.Windows;
using EnterpriseMVVMTemplate.Windows.ValidationRules;
using Api.Data.Contexts; using Api.Data.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels
{
    public class EditRoleViewModel : ViewModelBase
    {
        #region Fields
        private readonly Role _userRole;
        #endregion

        #region Contructor(s)
        public EditRoleViewModel(Role role)
        {

            if (role == null)
            {
                _userRole = new Role();
                IsNew = true;
            }
            else
            {
                _userRole = role;
                IsNew = false;
            }
            Status = string.Empty;

            //register validation rules
            //title
            ValidationRules.Add(new ValidationStringRule("Title", "RoleTitle")
            {
                MinLength = 4,
                MaxLength = 16
            });

            ValidatedProperties.Add("RoleTitle");
            SaveChangesCommand = new ActionCommand(SaveChanges, param => HasErrors == false);           
        }

        #endregion

        #region Commands
        public ActionCommand SaveChangesCommand
        {
            get; 
            private set;
        }

        public void SaveChanges(object obj)
        {
            _userRole.LastUpdatedById = LoggedInUser.Id;
            _userRole.LastUpdated = DateTime.Now;
            if (IsNew)
            {
                _userRole.CreatedById = LoggedInUser.Id;
                _userRole.CreationDate = DateTime.Now;
                Manager.AddRole(_userRole);
            }
            else
            {
                Manager.UpdateRole(_userRole);
            }
            Status = "Saved";
            OnPropertyChanged("Status");
            Close(null);
        }
        #endregion

        #region Methods
        #endregion

        #region Properties

        public bool IsNew { get; set; }

        public Role UserRole
        {
            get
            {
                return _userRole;
            }
        }

        [Required]
        [StringLength(32, MinimumLength = 4)] //32 == max length
        public string RoleTitle
        {
            get
            {
                return _userRole.RoleTitle;
            }
            set
            {
                _userRole.RoleTitle = value;
                OnPropertyChanged();
            }
        }

        public string Description
        {
            get
            {
                return _userRole.Description;
            }
            set
            {
                _userRole.Description = value;
                OnPropertyChanged();
            }
        }

        public string Status
        {
            get;
            set;
        }

        public string CreatedBy
        {
            get
            {
                User createdByUser = Manager.GetAllUsers().FirstOrDefault(us => us.Id == _userRole.CreatedById);
                if (createdByUser != null)
                {
                    return createdByUser.Username;
                }
                return "Not Specified";
            }
        }

        public string UpdatedBy
        {
            get
            {
                User updatedByUser = Manager.GetAllUsers().FirstOrDefault(us => us.Id == _userRole.LastUpdatedById);
                if (updatedByUser != null)
                {
                    return updatedByUser.Username;
                }
                return "Not Specified";
            }
        }

        public DateTime? CreatedDate
        {
            get
            {
                return _userRole.CreationDate;
            }
        }

        public DateTime? UpdatedDate
        {
            get
            {
                return _userRole.LastUpdated;
            }
        }
        #endregion
    }
}
