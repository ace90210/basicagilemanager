﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Windows.Documents;
using System.Windows.Markup;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels.DataViewModels;
using EnterpriseMVVMTemplate.Windows;
using EnterpriseMVVMTemplate.Windows.ValidationRules;
using System;
using System.Linq;
using Api.Data.Contexts; using Api.Data.Models;
using BasicAgileManager.AgileData.Models;

namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels
{
    public class EditStoryViewModel : ViewModelBase
    {
        #region Fields
        private Story _story;
        private StoryStatus _selectedStoryStatus;
        private User _selectedOwner;
        private ReleaseViewModel _selectedParentRelease;

        #endregion

        public EditStoryViewModel(Story story, List<ReleaseViewModel> releases)
        {
            StoryStatuses = new ObservableCollection<StoryStatus>(Manager.GetAllStoryStatuses());
            _users = new ObservableCollection<User>(Manager.GetAllUsers());
            _users.Insert(0, UserViewModel.NullUser);

            Releases = new ObservableCollection<ReleaseViewModel>(releases);
            if (story == null)
            {
                IsNew = true;
                _story = new Story();
                _story.Priority = 3;
                _story.OwnerId = LoggedInUser.Id;
                SelectedParentRelease = Releases.FirstOrDefault();
                SelectedStoryStatus = StoryStatuses.FirstOrDefault();
                SelectedOwner = UserViewModel.NullUser;
            }
            else
            {
                IsNew = false;
                _story = story;

                User createdUser = Manager.GetUserById(_story.CreatedById);
                CreatedBy = createdUser == null ? "Not Found" : createdUser.Username;

                if (_story.LastUpdatedById != null)
                {
                    User updatedUser = Manager.GetUserById((int)_story.LastUpdatedById);

                    UpdatedBy = updatedUser == null ? "Not Found" : updatedUser.Username;
                }

                SelectedParentRelease = releases.FirstOrDefault(r => r.Release.Id == _story.ReleaseId);
                SelectedStoryStatus = StoryStatuses.FirstOrDefault(ss => ss.Id == _story.StatusId);
                SelectedOwner = Users.FirstOrDefault(u => _story.OwnerId != null && u.Id == (int)_story.OwnerId);
            }


            //register validation rules
            //title
            ValidationRules.Add(new ValidationStringRule("Title", "Title")
            {
                MinLength = 4,
                MaxLength = 256
            });


            //register validation rules
            //description
            ValidationRules.Add(new ValidationStringRule("Description", "Description")
            {
                MinLength = 4
            });


            //parent project
            ValidationRules.Add(new ValidationRule("Parent Release", "SelectedParentRelease", ValidateParent) { UseCustomDelegateOnly = true });



            //status
            ValidationRules.Add(new ValidationRule("Status", "SelectedStoryStatus", ValidateStatus) { UseCustomDelegateOnly = true });


            SaveChangesCommand = new ActionCommand(SaveChanges, param => CanSave());
            SaveAndCloseCommand = new ActionCommand(SaveAndClose, param => CanSave());
            CloseCommand = new ActionCommand(CloseWindow, param => true);
        }

        private string ValidateParent()
        {
            const string errorDesc = "A TargetRelease must be selected";
            Errors.Remove(errorDesc);
            if (SelectedParentRelease == null ||
                SelectedParentRelease.Release == null ||
                SelectedParentRelease.Release.Id == 0)
            {
                Errors.Add(errorDesc);
                return errorDesc;
            }
            return null;
        }

        private string ValidateStatus()
        {
            const string errorDesc = "A Status must be selected";
            Errors.Remove(errorDesc);
            if (SelectedStoryStatus == null ||
                SelectedStoryStatus.Id == 0)
            {
                Errors.Add(errorDesc);
                return errorDesc;
            }
            return null;
        }
        
        #region Commands
        public ActionCommand SaveChangesCommand
        {
            get;
            private set;
        }

        public ActionCommand SaveAndCloseCommand
        {
            get;
            private set;
        }

        public ActionCommand CloseCommand
        {
            get;
            private set;
        }

        public bool CanSave()
        {
            if (HasErrors || Errors.Any())
            {
                return false;
            }
            return true;
        }

        public void SaveChanges(object obj = null)
        {

            if (IsNew)
            {
                _story.CreatedById = LoggedInUser.Id;
                _story.CreationDate = DateTime.Now;
                //add release
                Manager.AddStory(_story);
                IsNew = false;
                WasAdded = true;
            }
            else
            {
                //update models last updated fields
                _story.LastUpdatedById = LoggedInUser.Id;
                _story.LastUpdated = DateTime.Now;
                //update release
                Manager.UpdateStory(_story);
            }

            Status = "Saved";
            OnPropertyChanged("Status");
        }

        public void SaveAndClose(object obj = null)
        {
            SaveChanges(obj);
            Close(null);
        }

        public void CloseWindow(object obj = null)
        {
            Close(null);
        }
        #endregion

        #region Validation

        protected override string OnValidate(string propertyName)
        {
            string error = base.OnValidate(propertyName);

            OnPropertyChanged("Status");

            return error;
        }

        #endregion

        #region Properties

        public bool IsNew { get; set; }

        private string _status;
        public string Status
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_status) && Errors.Any())
                {
                    return Errors.FirstOrDefault();
                }
                return _status;

            }
            private set
            {
                _status = value;
                OnPropertyChanged();
            }
        }

        public string Title
        {
            get
            {
                return _story.Title;
            }
            set
            {
                if (_story != null)
                {
                    _story.Title = value;
                }
                OnPropertyChanged();
            }
        }

        public string Description
        {
            get
            {
                return _story.Description;
            }
            set
            {
                if (_story != null)
                {
                    _story.Description = value;
                }
                OnPropertyChanged();
            }
        }

        public long? TfsId
        {
            get
            {
                return _story.TFSID;
            }
            set
            {
                if (_story != null)
                {
                    _story.TFSID = value;
                }
                OnPropertyChanged();
            }
        }


        public long? ChangeSet
        {
            get
            {
                return _story.ChangeSet;
            }
            set
            {
                if (_story != null)
                {
                    _story.ChangeSet = value;
                }
                OnPropertyChanged();
            }
        }

        public string DisplayDescription
        {
            get
            {
                if (_story != null && _story.Description != null)
                {
                    var stream = new MemoryStream(Encoding.UTF8.GetBytes(_story.Description));
                    var fd = (FlowDocument) XamlReader.Load(stream);
                    return new TextRange(fd.ContentStart, fd.ContentEnd).Text;
                }
                return string.Empty;
            }
        }

        public int Priority
        {
            get { return _story.Priority; }
            set { _story.Priority = value; }
        }

        public string CreatedBy { get; private set; }
        public string UpdatedBy { get; private set; }

        public DateTime? CreatedDate
        {
            get
            {
                return _story.CreationDate;
            }
        }
        public DateTime? UpdatedDate
        {
            get
            {
                if (_story.LastUpdated != null)
                    return _story.LastUpdated.Value;
                return null;
            }
        }

        private ObservableCollection<User> _users;
        public ObservableCollection<User> Users
        {
            get { return _users; }
            set
            {
                _users = value; 
                OnPropertyChanged(); 
            }
        }

        public User SelectedOwner
        {
            get
            {
                return _selectedOwner;
            }
            set
            {
                if (value != null)
                {
                    _selectedOwner = value;
                }
                else
                {
                    _selectedOwner = UserViewModel.NullUser;
                }
                OnPropertyChanged();
                if (_selectedOwner != null)
                {
                    if (_selectedOwner.Id == -1)
                    {
                        Story.OwnerId = null;
                    }
                    else
                    {
                        Story.OwnerId = _selectedOwner.Id;
                    }
                }
            }
        }

        public ObservableCollection<StoryStatus> StoryStatuses
        {
            get;
            set;
        }

        public StoryStatus SelectedStoryStatus
        {
            get
            {
                return _selectedStoryStatus;
            }
            set
            {
                _selectedStoryStatus = value;
                OnPropertyChanged();
                if (_selectedStoryStatus != null)
                {
                    Story.StatusId = _selectedStoryStatus.Id;
                }
            }
        }


        public Iteration Iteration
        {
            get
            {
                return _story.Iteration;
            }
            set
            {
                _story.Iteration = value;
                if (value != null)
                {
                    _story.IterationId = value.Id;
                }
                OnPropertyChanged();
            }
        }


        public ObservableCollection<ReleaseViewModel> Releases
        {
            get;
            set;
        }

        public ReleaseViewModel SelectedParentRelease
        {
            get
            {
                return _selectedParentRelease;
            }
            set
            {
                _selectedParentRelease = value;
                OnPropertyChanged();
                if (_selectedParentRelease != null)
                {
                    Story.ReleaseId = _selectedParentRelease.Release.Id;
                }
            }
        }

        public bool WasAdded { get; set; }

        public Story Story
        {
            get { return _story; }
            set
            {
                _story = value;
                OnPropertyChanged();
            }
        }

        #endregion
    }
}