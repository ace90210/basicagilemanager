﻿using EnterpriseMVVMTemplate.DesktopClient.@base;
using EnterpriseMVVMTemplate.DesktopClient.EventTypes;
using EnterpriseMVVMTemplate.DesktopClient.Helpers;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels.DataViewModels;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels.TreeItemViewModels;
using EnterpriseMVVMTemplate.Windows;
using Api.Data.Contexts; using Api.Data.Models;
using System.Collections.ObjectModel;
using System.Linq;
using Microsoft.Practices.Prism.PubSubEvents;
using Xceed.Wpf.AvalonDock.Themes;

namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels
{
    public struct ThemeRow
    {
        public ThemeRow(Theme t, string title)
        {
            ThemeItem = t;
            _title = title;
        }
        public readonly Theme ThemeItem;
        private readonly string _title;

        public string Title
        {
            get { return _title; }
        }
    }

    public class IterationModuleViewModel : ViewModelBase
    {
        #region Fields
        private Project _selectedProject;
        private Release _selectedRelease;
        private ObservableCollection<BaseTreeViewItemObject> _projectList;
        private ObservableCollection<Story> _stories;
        private readonly ObservableCollection<ThemeRow> _themes;

        #endregion

        #region Contructor(s)
        public IterationModuleViewModel()
        {
            LoadData();
            ProjectList = new ObservableCollection<BaseTreeViewItemObject>(Projects.Select(p => new ProjectTreeViewItemViewModel(p.Title, new ProjectViewModel(p.Project))));
            
            RefreshCommand = new ActionCommand(o => Refresh(false), param => true);
        
            CurrentView = this;
            _themes = new ObservableCollection<ThemeRow>();
            ThemeRow nullRow = new ThemeRow(null, "Not Specified");
            ThemeRow aeroTheme = new ThemeRow(new AeroTheme(), "Aero Theme");
            _selectedTheme = aeroTheme;
            _themes.Add(nullRow);
            _themes.Add(aeroTheme);
            _themes.Add(new ThemeRow(new GenericTheme(), "Generic Theme"));
            _themes.Add(new ThemeRow(new MetroTheme(), "Metro Theme"));
            _themes.Add(new ThemeRow(new VS2010Theme(), "VS2010 Theme"));

            IEventAggregator eventAggregator = CustomEventManager.Instance;
            eventAggregator.GetEvent<ReleaseSelectionChanged>().Subscribe(r =>
            {
                if (r != null)
                {
                    if (r is ProjectViewModel)
                    {
                        _selectedProject = (r as ProjectViewModel).Project;
                        _selectedRelease = null;
                    }
                    else if (r is ReleaseViewModel)
                    {
                        _selectedRelease = (r as ReleaseViewModel).Release;
                        _selectedProject = _selectedRelease.Project;
                    }
                    else if (r is IterationViewModel)
                    {
                        _selectedRelease = (r as IterationViewModel).Release;
                        _selectedProject = _selectedRelease.Project;
                    }
                    else if (r is StoryViewModel)
                    {
                        _selectedRelease = (r as StoryViewModel).Iteration.Release;
                        _selectedProject = _selectedRelease.Project;
                    }
                }

                if (_selectedProject == null)
                {
                    Stories =
                        new ObservableCollection<EditStoryViewModel>(
                            _stories.Where(s => s.IterationId == null).Select(s => new EditStoryViewModel(s, Releases.ToList())));
                }
                else
                {
                    if (_selectedRelease != null)
                    {
                        Stories =
                            new ObservableCollection<EditStoryViewModel>(
                                _stories.Where(s => _selectedRelease.Id == s.ReleaseId && s.IterationId == null)
                                    .Select(s => new EditStoryViewModel(s, Releases.Where(rvm => rvm.Release == _selectedRelease).ToList())));
                    }
                    else
                    {
                        Stories =
                            new ObservableCollection<EditStoryViewModel>(
                                    _stories.Where(s => s.Release.Project.Id == _selectedProject.Id && s.IterationId == null)
                                    .Select(s => new EditStoryViewModel(s, Releases.ToList())));

                    }
                }

                OnPropertyChanged("Projects");
                OnPropertyChanged("ProjectList");
                OnPropertyChanged("Releases");
                OnPropertyChanged("Stories");
            });
        }

        #endregion

        #region Commands
        public ActionCommand RefreshCommand
        {
            get;
            private set;
        }

        public void CompleteRefresh()
        {
            Refresh(true);
            ProjectList = new ObservableCollection<BaseTreeViewItemObject>(Projects.Select(p => new ProjectTreeViewItemViewModel(p.Title, new ProjectViewModel(p.Project))));
            
        }

        public void LoadData()
        {
            Roles = new ObservableCollection<Role>(Manager.GetAllRoles());
            Users = new ObservableCollection<User>(Manager.GetAllUsers());
            Projects = new ObservableCollection<ProjectViewModel>(Manager.GetAllProjects().Select(p => new ProjectViewModel(p)));
            
            _stories = new ObservableCollection<Story>(Manager.GetAllStories());

        }

        public override void Refresh(bool renewContext)
        {
            base.Refresh(renewContext);

            LoadData();
            BaseTreeViewItemObject selectedTreeViewItem = null;

            foreach (BaseTreeViewItemObject item in ProjectList)
            {
                BaseTreeViewItemObject tmp = GetSelectedTreeViewItemObject(item);
                if (tmp != null)
                {
                    selectedTreeViewItem = tmp;
                    break;
                }
            }

            //handle selected item is a project
            if (selectedTreeViewItem != null && selectedTreeViewItem.Item != null &&
                selectedTreeViewItem.Item is ProjectViewModel)
            {
                SelectedProject = (selectedTreeViewItem.Item as ProjectViewModel).Project;
                SelectedRelease = null;
                //if (SelectedRelease == null || SelectedRelease.Project != SelectedProject)
                //{
                //    ReleaseViewModel editReleaseViewModel = Releases.FirstOrDefault(r => r.Project == _selectedProject);
                //    if (editReleaseViewModel != null)
                //    {
                //        Release selectedRelease = editReleaseViewModel.Release;
                //        if (selectedRelease != null)
                //            SelectedRelease = selectedRelease;
                //    }
                //}
            }

            //handle selected item is a release
            if (selectedTreeViewItem != null && selectedTreeViewItem.Item != null &&
                selectedTreeViewItem.Item is ReleaseViewModel)
            {
                SelectedProject = (selectedTreeViewItem.Item as ReleaseViewModel).Project;
                SelectedRelease = (selectedTreeViewItem.Item as ReleaseViewModel).Release;
            }

            if (_selectedProject == null)
            {
                Stories =
                    new ObservableCollection<EditStoryViewModel>(
                        _stories.Where(s => s.IterationId == null).Select(s => new EditStoryViewModel(s, Releases.ToList())));
            }
            else
            {
                if (_selectedRelease != null)
                {
                    Stories =
                        new ObservableCollection<EditStoryViewModel>(
                            _stories.Where(s => _selectedRelease.Id == s.ReleaseId && s.IterationId == null)
                                .Select(s => new EditStoryViewModel(s, Releases.Where(rvm => rvm.Release == _selectedRelease).ToList())));
                }
                else
                {
                    Stories =
                        new ObservableCollection<EditStoryViewModel>(
                                _stories.Where(s => s.Release.Project.Id == _selectedProject.Id && s.IterationId == null)
                                .Select(s => new EditStoryViewModel(s, Releases.ToList())));
                    
                }
            }

            OnPropertyChanged("Roles");
            OnPropertyChanged("Users");
            OnPropertyChanged("Projects");
            OnPropertyChanged("ProjectList");
            OnPropertyChanged("Releases");
            OnPropertyChanged("Stories");
        }
        #endregion

        #region Methods

        private BaseTreeViewItemObject GetSelectedTreeViewItemObject(BaseTreeViewItemObject root)
        {
            if (root == null)
                return null;

            if (root.IsSelected)
                return root;

            if (root.Children != null)
            {
                foreach (BaseTreeViewItemObject child in root.Children)
                {
                    BaseTreeViewItemObject tmp = GetSelectedTreeViewItemObject(child);
                    if (tmp != null)
                    {
                        return tmp;
                    }
                }
            }
            return null;
        }
        #endregion

        #region Properties

        public ViewModelBase CurrentView { get; set; }

        public string WelcomeMessage
        {
            get
            {
                if(StaticDataController.LoggedInUser != null)
                    return "Welcome online: " + StaticDataController.LoggedInUser.Username;

                return "Unknown user online";
            }
        }

        public ObservableCollection<Role> Roles
        {
            get;
            set;
        }

        public ObservableCollection<User> Users
        {
            get; 
            set;
        }

        public ObservableCollection<ProjectViewModel> Projects
        {
            get;
            set;
        }

        public ObservableCollection<ReleaseViewModel> Releases
        {
            get
            {
                if (_selectedProject == null)
                {
                    return new ObservableCollection<ReleaseViewModel>(
                        Manager.GetAllReleases().Select(r => new ReleaseViewModel(r)));
                }
                ObservableCollection<ReleaseViewModel> rel = new ObservableCollection<ReleaseViewModel>();
                foreach (BaseTreeViewItemObject project in ProjectList)
                {
                    foreach (BaseTreeViewItemObject releaseTreeObject in project.Children)
                    {
                        ReleaseViewModel release = releaseTreeObject.Item as ReleaseViewModel;
                        if (release != null && release.Project == _selectedProject)
                        {
                            rel.Add(release);
                        }
                    }
                }
                return rel;
            }

        }



        public ObservableCollection<BaseTreeViewItemObject> ProjectList
        {
            get { return _projectList; }
            private set
            {
                _projectList = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<EditStoryViewModel> Stories
        {
            get;
            set;
        }

        public Project SelectedProject
        {
            get
            {
                return _selectedProject;
            }
            set
            {
                _selectedProject = value; 
                OnPropertyChanged("ReleaseList");
                OnPropertyChanged();
            }
        }

        public Release SelectedRelease
        {
            get
            {
                return _selectedRelease;
            }
            set
            {
                if (_selectedRelease != value)
                {
                    _selectedRelease = value;
                    OnPropertyChanged();
                }
            }
        }

        public ObservableCollection<ThemeRow> ThemesList
        {
            get { return _themes; }
        }

        private ThemeRow _selectedTheme;

        public ThemeRow SelectedThemeRow
        {
            get { return _selectedTheme; }
            set
            {
                _selectedTheme = value;
                OnPropertyChanged();
                OnPropertyChanged("SelectedTheme");
            }
        }
        public Theme SelectedTheme
        {
            get { return _selectedTheme.ThemeItem; }
        }

        #endregion
    }
}
