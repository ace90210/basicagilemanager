﻿using EnterpriseMVVMTemplate.DesktopClient.ViewModels.DataViewModels;
using EnterpriseMVVMTemplate.Windows;
using Api.Data.Contexts; using Api.Data.Models;

namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        #region Fields
        private Project _selectedProject;
        private Release _selectedRelease;
        #endregion

        #region Contructor(s)
        public MainWindowViewModel()
        {
            _selectedProject = ProjectViewModel.NullProject;
            _selectedRelease = ReleaseViewModel.NullRelease;

            RefreshCommand = new ActionCommand((o) => Refresh(false), param => true);
            CurrentView = this;
        }

        #endregion

        #region Commands
        public ActionCommand RefreshCommand
        {
            get;
            private set;
        }

        public override void Refresh(bool renewContext)
        {
 	         base.Refresh(renewContext);
        }
        #endregion

        #region Methods
        #endregion

        #region Properties

        public ViewModelBase CurrentView { get; set; }

        public string WelcomeMessage
        {
            get
            {
                if(StaticDataController.LoggedInUser != null)
                    return "Welcome online: " + StaticDataController.LoggedInUser.Username;

                return "Unknown user online";
            }
        }
        #endregion
    }
}
