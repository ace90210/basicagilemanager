﻿using System;
using System.Linq;
using System.Security;
using EnterpriseMVVMTemplate.Windows;
using Api.Data.Contexts; using Api.Data.Models;

namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels
{
    public class OptionsViewModel : ViewModelBase
    {
        public OptionsViewModel(string status = null)
        {
            Status = status;
            _user = StaticDataController.LoggedInUser;


            SavePasswordChangesCommand = new ActionCommand(SavePasswordChanges, param => true);      
        }


        #region Properties
        public event EventHandler PostChangedPasswordEvent;

        public string PasswordChangeStatus { get; private set; }
        public string Status { get; private set; }

        public SecureString NewPassword { get; set; }

        public SecureString OldPassword { get; set; }

        private User _user;
        public User User
        {
            get
            {
                return _user;
            }

            set
            {
                _user = value;
            }
        }
        #endregion

        #region Commands
        public ActionCommand SavePasswordChangesCommand
        {
            get;
            private set;
        }

        public void SavePasswordChanges(object obj)
        {
            //check old password valid
            byte[] salt = _user.SecureSalt.ToArray();
            string hashedPassword = DataSecurityManager.HashString(OldPassword,
                        DataSecurityManager.Cypher.Default, ref salt);

            if (_user.Password == hashedPassword)
            {
                //if valid save new password, clear existing salt data
                salt = new byte[0];
                string pass = DataSecurityManager.HashString(NewPassword, DataSecurityManager.Cypher.Default, ref salt);
                _user.SecureSalt = salt;
                _user.Password = pass; //update models last updated fields
                _user.LastUpdatedById = LoggedInUser.Id;
                _user.LastUpdated = DateTime.Now;

                Manager.UpdateUser(_user);

                //update logged in user
                StaticDataController.LoggedInUser = _user;
                OldPassword.Clear();
                NewPassword.Clear();
                PasswordChangeStatus = "Password updated!";
            }
            else
            {
                PasswordChangeStatus = "Invalid old password";
            }

            if (PostChangedPasswordEvent != null)
            {
                PostChangedPasswordEvent(this, null);
            }
            OnPropertyChanged("PasswordChangeStatus");
        }
        #endregion
    }
}