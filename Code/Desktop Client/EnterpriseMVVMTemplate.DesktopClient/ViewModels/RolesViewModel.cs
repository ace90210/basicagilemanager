﻿using EnterpriseMVVMTemplate.Windows;
using Api.Data.Contexts; using Api.Data.Models;
using System.Collections.ObjectModel;

namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels
{
    public class RolesViewModel : ViewModelBase
    {
        #region Fields
        private ObservableCollection<Role> _userRoles;
        #endregion

        #region Contructor(s)
        public RolesViewModel()
        {

            _userRoles = new ObservableCollection<Role>(Manager.GetAllRoles());
        }

        #endregion
        
        #region Properties

        public ObservableCollection<Role> Roles 
        {
            get { return _userRoles; }
            set { _userRoles = value; }
        } 
        #endregion
    }
}
