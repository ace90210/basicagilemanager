﻿using System.Collections.ObjectModel;
using System.Linq;
using EnterpriseMVVMTemplate.DesktopClient.@base;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels.DataViewModels;

namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels.TreeItemViewModels
{

    namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels
    {
        public class IterationTreeViewItemViewModel : BaseTreeViewItemObject
        {
            private IterationViewModel iteration;

            public IterationTreeViewItemViewModel(string title, IterationViewModel iteration)
                : base(title, iteration)
            {
                this.iteration = iteration;
            }

            public override ObservableCollection<BaseTreeViewItemObject> Children
            {
                get
                {
                    return new ObservableCollection<BaseTreeViewItemObject>(iteration.Iteration.Stories.Select(i => new StoryTreeViewItemViewModel(i.Title, new StoryViewModel(i)))); 
                }
            }
        }
    }

}
