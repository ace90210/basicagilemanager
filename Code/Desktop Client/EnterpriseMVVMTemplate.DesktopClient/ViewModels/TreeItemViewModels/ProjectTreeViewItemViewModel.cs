﻿using System.Collections.ObjectModel;
using System.Linq;
using EnterpriseMVVMTemplate.DesktopClient.@base;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels.DataViewModels;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels.TreeItemViewModels.EnterpriseMVVMTemplate.DesktopClient.ViewModels;

namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels.TreeItemViewModels
{
    public class ProjectTreeViewItemViewModel : BaseTreeViewItemObject
    {

        private ObservableCollection<BaseTreeViewItemObject> _releaseList;

        public ProjectTreeViewItemViewModel(string title, ProjectViewModel project)
            : base(title, project)
        {
            _releaseList = new ObservableCollection<BaseTreeViewItemObject>(project.Releases.Select(r => new ReleaseTreeViewItemViewModel(new ReleaseViewModel(r))));
        }

        public override ObservableCollection<BaseTreeViewItemObject> Children
        {
            get { return new ObservableCollection<BaseTreeViewItemObject>(_releaseList); }
        }
    }
}
