﻿using System.Collections.ObjectModel;
using System.Linq;
using EnterpriseMVVMTemplate.DesktopClient.@base;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels.DataViewModels;

namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels.TreeItemViewModels
{

    namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels
    {
        public class ReleaseTreeViewItemViewModel : BaseTreeViewItemObject
        {

            private ObservableCollection<BaseTreeViewItemObject> _iterationList;

            public ReleaseTreeViewItemViewModel(ReleaseViewModel release) : base(release.Title, release)
            {
                _iterationList = new ObservableCollection<BaseTreeViewItemObject>(release.Release.Iterations.Select(i => new IterationTreeViewItemViewModel(i.Title, new IterationViewModel(i))));
                
            }

            public override ObservableCollection<BaseTreeViewItemObject> Children
            {
                get { return new ObservableCollection<BaseTreeViewItemObject>(_iterationList); }
            }
        }
    }

}
