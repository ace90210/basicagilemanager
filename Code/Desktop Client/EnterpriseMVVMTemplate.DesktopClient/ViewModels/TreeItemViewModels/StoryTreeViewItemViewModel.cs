﻿using EnterpriseMVVMTemplate.DesktopClient.@base;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels.DataViewModels;

namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels.TreeItemViewModels
{

    namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels
    {
        public class StoryTreeViewItemViewModel : BaseTreeViewItemObject
        {
            public StoryTreeViewItemViewModel(string title, StoryViewModel story)
                : base(title, story)
            {

            }
        }
    }

}
