﻿using EnterpriseMVVMTemplate.Windows.ValidationRules;
using System.ComponentModel.DataAnnotations;
using System.Security;

namespace EnterpriseMVVMTemplate.DesktopClient.ViewModels
{
    using Api.Data.Contexts; using Api.Data.Models;
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Windows;

    public class UserViewModel : ViewModelBase
    {
        public static readonly User NullUser = new User { Id = -1, Name = Strings.NotSpecified };

        #region Contructor(s)

        public UserViewModel(User user)
        {
            Roles = new ObservableCollection<Role>(Manager.GetAllRoles());
            Status = string.Empty;

            if (user == null)
            {
                IsNew = true;

                _user = new User();

                //added not specified role at top
                Role nullRole = new Role {RoleTitle = "Please select a user role."};
                Roles.Insert(0, nullRole);
                _selectedRole = nullRole;
            }
            else
            {
                IsNew = false;
                _user = user;

                //update properties from user fields
                Title = _user.Title;
                Name = _user.Name;
                Username = _user.Username;
                Description = _user.Description;
                AddressLine1 = _user.AddressLineOne;
                AddressLine2 = _user.AddressLineTwo;
                Town = _user.Town;
                PostCode = _user.Postcode;
                Telephone = _user.Telephone;
                Email = _user.Email;
                Mobile = _user.Mobile;
                RoleId = _user.RoleId;

                _selectedRole = Roles.FirstOrDefault(ro => ro.Id == user.RoleId);

                if (user.CreatedById != null)
                {
                    User createdUser = Manager.GetUserById(user.CreatedById);
                    CreatedBy = createdUser == null ? "Not Found" : createdUser.Username;
                }
                else
                {
                    CreatedBy = "Not Specified";
                }

                if (user.LastUpdatedById != null)
                {
                    User updatedUser = Manager.GetUserById(user.LastUpdatedById.Value);
                    UpdatedBy = updatedUser == null ? "Not Found" : updatedUser.Username;
                }
                else
                {
                    UpdatedBy = "Not Specified";
                }
            }

            //register validation rules

            if (IsNew)
            {
                //username (new specifc rules)
                ValidationRules.Add(new ValidationStringRule("Username", "Username")
                {
                    CaseRule = CaseRuleType.LowerCaseOnly,
                    InvalidValues = Manager.GetAllUsers().Select(us => us.Username).ToList(),
                    OverrideInvalidValuesMessage = "Username already in use"
                }); 
               
            }

            //username
            ValidationRules.Add(new ValidationStringRule("Username", "Username", "^[a-z0-9_-]{3,32}$")
            {
                MinLength = 4,
                MaxLength = 16
            }); 

            //Title
            ValidationRules.Add(new ValidationStringRule("Title", "Title"));

            //name
            ValidationRules.Add(new ValidationStringRule("Name", "Name")
            {
                MinLength = 4,
                MaxLength = 16

            });

            //role
            ValidationRules.Add(new ValidationRule("Role", "SelectedRole", ValidateRole) { UseCustomDelegateOnly = true }); 
            
            //register command
            SaveChangesCommand = new ActionCommand(SaveChanges, param => HasErrors == false && !Errors.Any());           
        }
        #endregion

        #region Properties


        public ObservableCollection<Role> Roles
        {
            get;
            set;
        }

        private Role _selectedRole;
        public Role SelectedRole
        {
            get
            {
                return _selectedRole;
            }
            set
            {
                _selectedRole = value;
                OnPropertyChanged();
                if (_selectedRole != null)
                {
                    RoleId = _selectedRole.Id;
                }
            }
        }

        private User _user;
        public User User
        {
            get
            {
                return _user;
            }

            set
            {
                _user = value;
            }
        }

        private string _status;
        public string Status
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_status) && Errors.Any())
                {
                    return Errors.FirstOrDefault();
                }
                return _status;
                
            }
            private set
            {
                _status = value;
                OnPropertyChanged();
            }
        }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Town { get; set; }
        public string PostCode { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }

        public SecureString Password { get; set; }

        public int RoleId { get; set; }

        public string CreatedBy { get; private set; }
        public string UpdatedBy { get; private set; }


        public DateTime? CreatedDate {
            get
            {
                if (_user.CreationDate != null) 
                    return _user.CreationDate;
                return null;
            }
        }
        public DateTime? UpdatedDate {
            get
            {
                if (_user.LastUpdated != null) 
                    return _user.LastUpdated.Value;
                return null;
            }
        }

        public bool IsNew { get; set; }

        [Required]
        [StringLength(32, MinimumLength = 4)]
        public string Username { get; set; }

        //[Required]
        //[StringLength(32, MinimumLength = 4)] 
        public string Name { get; set; }

        #endregion

        #region Commands
        public ActionCommand SaveChangesCommand
        {
            get;
            private set;
        }

        public void SaveChanges(object obj)
        {
            //update models last updated fields
            //_user.LastUpdatedById = LoggedInUser.Id;
            _user.LastUpdated = DateTime.Now;
            
            //update fields from properties
            _user.Title = Title;
            _user.Name = Name;
            _user.Username = Username;
            _user.Description = Description;
            _user.AddressLineOne = AddressLine1;
            _user.AddressLineTwo = AddressLine2;
            _user.Town = Town;
            _user.Postcode = PostCode;
            _user.Telephone = Telephone;
            _user.Email = Email;
            _user.Mobile = Mobile;
            _user.RoleId = RoleId;

            if (IsNew)
            {
                byte[] salt = new byte[0];
                string pass = DataSecurityManager.HashString(Password, DataSecurityManager.Cypher.Default, ref salt);
                _user.SecureSalt = salt;
                _user.Password = pass; //update models last updated fields
                //_user.CreatedById = LoggedInUser.Id;
                _user.CreationDate = DateTime.Now;
                Manager.AddUser(_user);
            }
            else
            {
                Manager.UpdateUser(_user);
            }
            Status = "Saved";
            OnPropertyChanged("Status");
            Close(null);
        }
        #endregion

        #region Validation

        protected override string OnValidate(string propertyName)
        {
            string error = base.OnValidate(propertyName);

            Status = error;
            OnPropertyChanged("Status");
            if (error == null)
            {
                return null;
            }
            return error;
        }
       
        private string ValidateRole()
        {
            const string errorDesc = "A Role must be selected";
            Errors.Remove(errorDesc);
            if (SelectedRole == null || SelectedRole.Id == 0)
            {
                Errors.Add(errorDesc);
                return errorDesc;
            }
            return null;
        }
        #endregion
    }
}
