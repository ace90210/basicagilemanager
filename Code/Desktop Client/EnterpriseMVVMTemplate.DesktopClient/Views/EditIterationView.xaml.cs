﻿using EnterpriseMVVMTemplate.DesktopClient.Base;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels;

namespace EnterpriseMVVMTemplate.DesktopClient.Views
{
    /// <summary>
    /// Interaction logic for EditIterationView.xaml
    /// </summary>
    public partial class EditIterationView 
    {
        public EditIterationView(EditIterationViewModel model, WindowBase parent)
            : base(model, parent)
        {
            InitializeComponent();
        }
    }
}
