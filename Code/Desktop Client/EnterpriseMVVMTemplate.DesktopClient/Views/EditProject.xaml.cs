﻿using EnterpriseMVVMTemplate.DesktopClient.Base;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels;

namespace EnterpriseMVVMTemplate.DesktopClient.Views
{
    /// <summary>
    /// Interaction logic for EditProjectView.xaml
    /// </summary>
    public partial class EditProjectView 
    {
        public EditProjectView(EditProjectViewModel model, WindowBase parent)
            : base(model, parent)
        {
            InitializeComponent();
        }
    }
}
