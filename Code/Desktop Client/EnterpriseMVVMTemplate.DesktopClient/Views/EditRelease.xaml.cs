﻿using EnterpriseMVVMTemplate.DesktopClient.Base;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels;

namespace EnterpriseMVVMTemplate.DesktopClient.Views
{
    /// <summary>
    /// Interaction logic for EditReleaseView.xaml
    /// </summary>
    public partial class EditReleaseView 
    {
        public EditReleaseView(EditReleaseViewModel model, WindowBase parent)
            : base(model, parent)
        {
            InitializeComponent();
        }
    }
}
