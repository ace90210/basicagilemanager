﻿using EnterpriseMVVMTemplate.DesktopClient.Base;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels;

namespace EnterpriseMVVMTemplate.DesktopClient.Views
{
    /// <summary>
    /// Interaction logic for EditRole.xaml
    /// </summary>
    public partial class EditRole
    {
        public EditRole(EditRoleViewModel model, WindowBase parent) : base(model, parent)
        {
            InitializeComponent();
        }
    }
}
