﻿using EnterpriseMVVMTemplate.DesktopClient.Base;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels;

namespace EnterpriseMVVMTemplate.DesktopClient.Views
{
    /// <summary>
    /// Interaction logic for EditStoryView.xaml
    /// </summary>
    public partial class EditStoryView 
    {
        public EditStoryView(EditStoryViewModel model, WindowBase parent)
            : base(model, parent)
        {
            InitializeComponent();
        }
    }
}
