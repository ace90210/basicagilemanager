﻿using EnterpriseMVVMTemplate.DesktopClient.Base;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels;
using System;
using System.Windows;
using System.Windows.Media;

namespace EnterpriseMVVMTemplate.DesktopClient.Views
{
    /// <summary>
    /// Interaction logic for EditUser.xaml
    /// </summary>
    public partial class EditUser
    {
        private Brush defaultBrush;

        public EditUser(UserViewModel context, WindowBase parent) : base(context, parent)
        {
            InitializeComponent();

            Loaded += UpdatePasswordValidation;
            defaultBrush = Password.BorderBrush;
        }

        private void Password_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            UpdatePasswordValidation(sender, e);
        }

        private void UpdatePasswordValidation(object sender, EventArgs e)
        {
            //if fields diabled ignore validation
            UserViewModel userViewModel = DataContext as UserViewModel;
            if (userViewModel == null || Password.IsEnabled == false)
            {
                return;
            }
            userViewModel.Password = Password.SecurePassword;

            if (string.IsNullOrWhiteSpace( Password.Password) || Password.Password != PasswordConfirm.Password)
            {
                //Execute code to alert user passwords don't match here.
                Password.BorderBrush = Brushes.Crimson;
                PasswordConfirm.BorderBrush = Brushes.Crimson;
                SaveButton.IsEnabled = false;
            }
            else
            {
                Password.BorderBrush = defaultBrush;
                PasswordConfirm.BorderBrush = defaultBrush;
                SaveButton.IsEnabled = true;
            }
        }

        private void EditUser_OnLoaded(object sender, RoutedEventArgs e)
        {
            UsernameTextBox.Focus();
        }
    }
}
