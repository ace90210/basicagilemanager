﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using EnterpriseMVVMTemplate.DesktopClient.Base;
using EnterpriseMVVMTemplate.DesktopClient.@base;
using EnterpriseMVVMTemplate.DesktopClient.EventTypes;
using EnterpriseMVVMTemplate.DesktopClient.Helpers;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels.DataViewModels;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels.TreeItemViewModels.EnterpriseMVVMTemplate.DesktopClient.ViewModels;
using EnterpriseMVVMTemplate.Windows;
using Xceed.Wpf.DataGrid;

namespace EnterpriseMVVMTemplate.DesktopClient.Views
{
    /// <summary>
    /// Interaction logic for EditUser.xaml
    /// </summary>
    public partial class IterationView
    {

        public IterationView(ViewModelBase baseViewModel)
            : base(baseViewModel)
        {
            InitializeComponent();
        }

        private void StoriesListView_OnMouseDoubleClick_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGridControl dataGridControl = sender as DataGridControl;
            if (dataGridControl != null)
            {
                EditStoryViewModel svm = dataGridControl.CurrentItem as EditStoryViewModel;
                if (svm != null)
                {
                    EditStoryView storyWindow = new EditStoryView(new EditStoryViewModel(svm.Story, svm.Releases.ToList()), Switcher.PageSwitcher);
                    storyWindow.ShowDialog();
                    StoriesView.Items.Refresh();
                    StoriesView.UpdateLayout();
                }
            }
        }

        private void TreeView_OnSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            IterationModuleViewModel ivm = DataContext as IterationModuleViewModel;
            if (ivm != null && TreeViewTest.SelectedItem is BaseTreeViewItemObject)
            {
                  //ivm.Refresh(null);

                CustomEventManager.Instance.GetEvent<ReleaseSelectionChanged>().Publish((TreeViewTest.SelectedItem as BaseTreeViewItemObject).Item);
            }
        }

        private void StoriesView_OnMouseMove(object sender, MouseEventArgs e)
        {
            DataRow backlog = sender as DataRow;
            if (backlog != null && e.LeftButton == MouseButtonState.Pressed)
            {
                DragDrop.DoDragDrop(backlog, StoriesView.SelectedItems, DragDropEffects.Move);
            }
        }

        private void IterationTreeView_OnDrop(object sender, DragEventArgs e)
        {
            TextBlock iterationTreeView = sender as TextBlock;

            if (iterationTreeView != null)
            {
                if (e.Data.GetDataPresent("Xceed.Wpf.DataGrid.SelectionItemCollection"))
                {
                    System.Collections.IList stories = (System.Collections.IList)e.Data.GetData("Xceed.Wpf.DataGrid.SelectionItemCollection");

                    foreach (var storyListItem in stories)
                    {
                        EditStoryViewModel story = storyListItem as EditStoryViewModel;
                        if (story != null)
                        {
                            BaseTreeViewItemObject baseTreeViewItemObject = iterationTreeView.DataContext as BaseTreeViewItemObject;
                            if (baseTreeViewItemObject != null)
                            {
                                IterationViewModel iterationViewModel = baseTreeViewItemObject.Item as IterationViewModel;
                                if (iterationViewModel != null)
                                    story.Iteration = iterationViewModel.Iteration;
                            }
                            story.SaveChanges();
                        }
                    }

                    IterationModuleViewModel iterationModuleViewModel = DataContext as IterationModuleViewModel;
                    if (iterationModuleViewModel != null)
                        iterationModuleViewModel.CompleteRefresh();
                    TreeViewTest.Items.Refresh();
                    TreeViewTest.UpdateLayout();
                }
            }
        }

        private void IterationTreeView_OnDragOver(object sender, DragEventArgs e)
        {
           
        }

        private void TreeViewTest_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender is TreeView && (sender as TreeView).SelectedItem is IterationTreeViewItemViewModel)
            {
                IterationTreeViewItemViewModel iterationTreeViewItemViewModel = (sender as TreeView).SelectedItem as  IterationTreeViewItemViewModel;
                if (iterationTreeViewItemViewModel != null)
                {
                    IterationViewModel ivm = iterationTreeViewItemViewModel.Item as IterationViewModel;
                    if (ivm != null)
                    {
                        IterationModuleViewModel iterationModuleViewModel = DataContext as IterationModuleViewModel;
                        if (iterationModuleViewModel != null)
                        {
                            EditIterationViewModel eivm = new EditIterationViewModel(ivm, iterationModuleViewModel.Releases);
                            {
                                EditIterationView iterationWindow = new EditIterationView(eivm, Switcher.PageSwitcher);
                                iterationWindow.ShowDialog();
                                StoriesView.Items.Refresh();
                                StoriesView.UpdateLayout();
                            }
                        }
                    }
                }
            }
        }
    }
}
