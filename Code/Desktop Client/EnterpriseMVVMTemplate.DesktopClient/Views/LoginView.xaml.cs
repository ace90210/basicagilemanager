﻿using System;
using System.Windows.Input;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels;
using EnterpriseMVVMTemplate.Windows;
using Api.Data.Contexts; using Api.Data.Models;
using System.Linq;
using System.Windows;
using Microsoft.Practices.Unity;

namespace EnterpriseMVVMTemplate.DesktopClient.Views
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login
    {
        public Login(string startStatus = "")
        {
            InitializeComponent();
            Loaded += Login_Loaded;
            StatusLabel.Content = startStatus;
            if (!string.IsNullOrWhiteSpace(startStatus))
            {
                StatusLabel.Visibility = Visibility.Visible;
            }

#if DEBUG
            ConnectionDatabaseComboBox.SelectedIndex = 1;
#endif
        }

        void Login_Loaded(object sender, RoutedEventArgs e)
        {
            UsernameTextBox.Focus();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            // Declare a Unity Container
            ViewModelBase.Container = new UnityContainer();

            ViewModelBase.Container.RegisterType<IBusinessContext, BusinessContext>();

            Cursor currentCursor = Mouse.OverrideCursor;
            Mouse.OverrideCursor = Cursors.Wait;

            //set connection setting
            if (ConnectionModeComboBox.SelectionBoxItem.Equals("Local"))
            {
                StaticDataController.ConnectionType = ConnectionTypeList.Local;
                BusinessContext.ConnectionType = ConnectionTypeList.Local;
            }
            else
            {
                StaticDataController.ConnectionType = ConnectionTypeList.Remote;
                BusinessContext.ConnectionType = ConnectionTypeList.Remote;
            }

            //set connection setting
            if (ConnectionDatabaseComboBox.SelectionBoxItem.Equals("Live"))
            {
                StaticDataController.ConnectionDatabase = ConnectionDatabaseList.Live;
                BusinessContext.ConnectionDatabase = ConnectionDatabaseList.Live;
            }
            else
            {
                StaticDataController.ConnectionDatabase = ConnectionDatabaseList.Development;
                BusinessContext.ConnectionDatabase = ConnectionDatabaseList.Development;
            }

#if DEBUG
            //if not entered password or not entered username bypass login authentication
            if (string.IsNullOrWhiteSpace(UsernameTextBox.Text) || PasswordTextBox.SecurePassword.Length == 0)
            {
                StatusLabel.Visibility = Visibility.Hidden;
                StatusLabel.Content = "";
                string username = string.IsNullOrWhiteSpace(UsernameTextBox.Text)
                    ? "ace90210"
                    : UsernameTextBox.Text;

                using (IBusinessContext bc = ViewModelBase.Container.Resolve<IBusinessContext>())
                {
                    StaticDataController.LoggedInUser =
                        bc.GetAllUsers()
                            .SingleOrDefault(us => us.Username == username);
                    if (StaticDataController.LoggedInUser == null)
                    {
                        StatusLabel.Visibility = Visibility.Visible;
                        StatusLabel.Content = "User not found.";

                        Mouse.OverrideCursor = currentCursor;
                        return;
                    }

                }
                IterationModuleViewModel ivm = new IterationModuleViewModel();
                MainWindow mainWindowd = new MainWindow(ivm);

                mainWindowd.Show();
                Close();

                Mouse.OverrideCursor = currentCursor;
                return;
            }

#endif
            if (string.IsNullOrWhiteSpace(UsernameTextBox.Text))
            {
                StatusLabel.Visibility = Visibility.Visible;
                StatusLabel.Content = "Please enter a username";
            }
            else if (PasswordTextBox.SecurePassword.Length == 0)
            {
                StatusLabel.Visibility = Visibility.Visible;
                StatusLabel.Content = "Please enter a password";
            }
            else
            { 
                try
                {
                    //attempt login
                   using (IBusinessContext bc = ViewModelBase.Container.Resolve<IBusinessContext>()) 
                   {
                        User user = bc.GetAllUsers().FirstOrDefault(us => us.Username == UsernameTextBox.Text);

                        if (user == null)
                        {
                            StatusLabel.Visibility = Visibility.Visible;
                            StatusLabel.Content = "Invalid username or password";
                            return;
                        }
                        byte[] salt = user.SecureSalt.ToArray();
                        string hashedPassword = DataSecurityManager.HashString(PasswordTextBox.SecurePassword,
                                    DataSecurityManager.Cypher.Default, ref salt);
                        bool exists =
                            bc.GetAllUsers().Any(us => us.Username == UsernameTextBox.Text && us.Password == hashedPassword);

                       if (!exists)
                       {
                           StatusLabel.Visibility = Visibility.Visible;
                           StatusLabel.Content = "Invalid username or password";
                       }
                       else
                       {
                           //logged in successfully
                           StatusLabel.Visibility = Visibility.Hidden;
                           StatusLabel.Content = "";
                           
                           StaticDataController.LoggedInUser = bc.GetAllUsers().SingleOrDefault(us => us.Username == UsernameTextBox.Text && us.Password == hashedPassword);
                           
                           
                            MainWindowViewModel mwvm = new MainWindowViewModel();
                            MainWindow mainWindow = new MainWindow(mwvm);

                            mainWindow.Show();
                           Close();
                        }

                    }
                }
                catch (Exception ex)
                {
                    string message = ex.Message;
                    if (ex.InnerException != null)
                    {
                        message = ex.InnerException.Message;
                    }
                    MessageBox.Show("Unable to connect to server due to following error:\n" + message);
                }
                finally
                {
                    Mouse.OverrideCursor = currentCursor;
                }
            }
        }

        private void OpenOfflineFile_OnClickButton_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
