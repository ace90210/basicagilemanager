﻿using System.Linq;
using EnterpriseMVVMTemplate.DesktopClient.Base;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels.DataViewModels;
using EnterpriseMVVMTemplate.Windows;
using System;
using System.Windows;
using System.Windows.Input;

namespace EnterpriseMVVMTemplate.DesktopClient.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow(ViewModelBase model = null) : base(model)
        {
            InitializeComponent();
            
            Switcher.PageSwitcher = this;
            Switcher.Switch(new IterationView(DataContext as IterationModuleViewModel));
        }

        private void AddRole_Click(object sender, RoutedEventArgs e)
        {
            EditRoleViewModel ervm = new EditRoleViewModel(null);
            EditRole editRoleView = new EditRole(ervm, this);
            ervm.CloseEvent += UpdateRoleList;
            editRoleView.ShowDialog();

        }

        public void UpdateRoleList(object sender, EventArgs e)
        {
            IterationModuleViewModel iterationModuleViewModel = DataContext as IterationModuleViewModel;
            if (iterationModuleViewModel != null)
            {
                EditRoleViewModel editRoleViewModel = sender as EditRoleViewModel;
                if (editRoleViewModel != null)
                    iterationModuleViewModel.Roles.Add(editRoleViewModel.UserRole);
            }
        }

        private void AddUser_Click(object sender, RoutedEventArgs e)
        {
            UserViewModel euvm = new UserViewModel(null);
            EditUser editRoleView = new EditUser(euvm, this);
            euvm.CloseEvent +=  UpdateUserList;
            editRoleView.ShowDialog();
        }

        public void UpdateUserList(object sender, EventArgs e)
        {
            IterationModuleViewModel iterationModuleViewModel = DataContext as IterationModuleViewModel;
            if (iterationModuleViewModel != null)
            {
                UserViewModel userViewModel = sender as UserViewModel;
                if (userViewModel != null)
                    iterationModuleViewModel.Users.Add(userViewModel.User);
            }
        }

        private void AddProject_Click(object sender, RoutedEventArgs e)
        {
            EditProjectViewModel pvm = new EditProjectViewModel(null);
            EditProjectView editProjectView = new EditProjectView(pvm, this);
            pvm.CloseEvent += UpdateProjectList;
            editProjectView.ShowDialog();
        }

        public void UpdateProjectList(object sender, EventArgs e)
        {
            IterationModuleViewModel iterationModuleViewModel = DataContext as IterationModuleViewModel;
            if (iterationModuleViewModel != null)
            {
                ProjectViewModel projectViewModel = sender as ProjectViewModel;
                if (projectViewModel != null)
                    iterationModuleViewModel.Projects.Add(projectViewModel);
            }
        }
        
        private void AddRelease_Click(object sender, RoutedEventArgs e)
        {
            IterationModuleViewModel iterationModuleViewModel = DataContext as IterationModuleViewModel;
            if (iterationModuleViewModel != null)
            {
                EditReleaseViewModel rvm = new EditReleaseViewModel(null, iterationModuleViewModel.Projects);
                EditReleaseView editReleaseView = new EditReleaseView(rvm, this);
                rvm.CloseEvent += UpdateReleaseList;
                editReleaseView.ShowDialog();
            }
        }

        public void UpdateReleaseList(object sender, EventArgs e)
        {
            IterationModuleViewModel iterationModuleViewModel = DataContext as IterationModuleViewModel;
            if (iterationModuleViewModel != null)
            {
                ReleaseViewModel releaseViewModel = sender as ReleaseViewModel;
                if (releaseViewModel != null)
                    iterationModuleViewModel.Releases.Add(releaseViewModel);
            }
        }


        private void AddIteration_Click(object sender, RoutedEventArgs e)
        {
            IterationModuleViewModel iterationModuleViewModel = DataContext as IterationModuleViewModel;
            if (iterationModuleViewModel != null)
            {
                EditIterationViewModel eivm = new EditIterationViewModel(null, iterationModuleViewModel.Releases);
                EditIterationView editIterationView = new EditIterationView(eivm, this);
                eivm.CloseEvent += UpdateIterationList;
                editIterationView.ShowDialog();
            }
        }

        public void UpdateIterationList(object sender, EventArgs e)
        {
            IterationModuleViewModel iterationModuleViewModel = DataContext as IterationModuleViewModel;
            if (iterationModuleViewModel != null)
            {
                EditIterationViewModel editIterationViewModel = sender as EditIterationViewModel;
                if (editIterationViewModel != null)
                {
                    ReleaseViewModel releaseViewModel = iterationModuleViewModel.Releases.FirstOrDefault( r=> r.Release == editIterationViewModel.SelectedRelease.Release);
                    if (
                        releaseViewModel != null)
                        releaseViewModel.Release.Iterations.Add(editIterationViewModel.Iteration);
                }
            }
        }


        private void AddStory_Click(object sender, RoutedEventArgs e)
        {
            IterationModuleViewModel iterationModuleViewModel = DataContext as IterationModuleViewModel;
            if (iterationModuleViewModel != null)
            {
                EditStoryViewModel svm = new EditStoryViewModel(null, iterationModuleViewModel.Releases.ToList());
                EditStoryView editReleaseView = new EditStoryView(svm, this);
                svm.CloseEvent += UpdateStoryList;
                editReleaseView.ShowDialog();
            }
        }

        public void UpdateStoryList(object sender, EventArgs e)
        {
            IterationModuleViewModel iterationModuleViewModel = DataContext as IterationModuleViewModel;
            if (iterationModuleViewModel != null)
            {
                EditStoryViewModel storyViewModel = sender as EditStoryViewModel;
                if (storyViewModel != null && storyViewModel.WasAdded)
                    iterationModuleViewModel.Stories.Add(storyViewModel);
            }
        }

        private void MainWindow_OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F5)
            {
                IterationModuleViewModel windowViewModel = DataContext as IterationModuleViewModel;
                if (windowViewModel != null)
                {
                    windowViewModel.Refresh(true);
                }
            }
        }

        private void ExitButton_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Logout_OnClick(object sender, RoutedEventArgs e)
        {

            StaticDataController.LoggedInUser = null;

            Login loginWindow = new Login("Logged out successfully.");

            loginWindow.Show();
            Close();
        }

        private void Options_OnClick(object sender, RoutedEventArgs e)
        {
            OptionsViewModel ovm = new OptionsViewModel();
            OptionsView optionsView = new OptionsView(ovm, this);
            optionsView.ShowDialog();
        }


        private void NavigateToRoles_Clicked(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new RolesView(new RolesViewModel()));
        }

        private void NavigateToUser_Clicked(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new UserView(new UserViewModel(null)));
        }

        private void NavigateToHome_Clicked(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new IterationView(DataContext as IterationModuleViewModel));
        }
    }
}
