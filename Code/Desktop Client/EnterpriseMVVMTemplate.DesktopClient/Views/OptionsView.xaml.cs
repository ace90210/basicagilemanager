﻿using System;
using System.Windows;
using System.Windows.Media;
using EnterpriseMVVMTemplate.DesktopClient.Base;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels;

namespace EnterpriseMVVMTemplate.DesktopClient.Views
{
    /// <summary>
    /// Interaction logic for OptionsView.xaml
    /// </summary>
    public partial class OptionsView
    {
        private Brush defaultBrush;
        public OptionsView(OptionsViewModel model, WindowBase parent) : base(model, parent)
        {
            InitializeComponent();
            Loaded += UpdatePasswordValidation;
            defaultBrush = OldPasswordBox.BorderBrush;
        }

        private void Password_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            UpdatePasswordValidation(sender, e);
            OptionsViewModel optionsViewModel = DataContext as OptionsViewModel;

            if (optionsViewModel != null)
            {
                optionsViewModel.PostChangedPasswordEvent += OptionsViewModelOnPostChangedPasswordEvent;
            }
        }

        private void OptionsViewModelOnPostChangedPasswordEvent(object sender, EventArgs eventArgs)
        {
            OldPasswordBox.Clear();
            Password.Clear();
            PasswordConfirm.Clear();
        }

        private void UpdatePasswordValidation(object sender, EventArgs e)
        {
            OptionsViewModel optionsViewModel = DataContext as OptionsViewModel;

            if (optionsViewModel != null)
            {
                optionsViewModel.OldPassword = OldPasswordBox.SecurePassword.Copy();
                optionsViewModel.NewPassword = Password.SecurePassword.Copy();

                //check old password not empty
                if (OldPasswordBox.SecurePassword.Length == 0)
                {
                    //Execute code to alert user passwords don't match here.
                    OldPasswordBox.BorderBrush = Brushes.Crimson;
                    UpdatePasswordButton.IsEnabled = false;
                }
                //check new passwords match
                else if (string.IsNullOrWhiteSpace(Password.Password) || Password.Password != PasswordConfirm.Password)
                {
                    //current password valid so reset colour
                    OldPasswordBox.BorderBrush = defaultBrush.Clone();

                    //new passwords dont match/invalid
                    Password.BorderBrush = Brushes.Crimson;
                    PasswordConfirm.BorderBrush = Brushes.Crimson;
                    UpdatePasswordButton.IsEnabled = false;
                }
                else
                {
                    //everything fine
                    OldPasswordBox.BorderBrush = defaultBrush.Clone();
                    Password.BorderBrush = defaultBrush.Clone();
                    PasswordConfirm.BorderBrush = defaultBrush.Clone();
                    UpdatePasswordButton.IsEnabled = true;
                }
            }
        }
    }
}
