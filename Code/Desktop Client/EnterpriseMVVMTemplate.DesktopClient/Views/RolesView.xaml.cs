﻿using System.Windows.Controls;
using System.Windows.Input;
using EnterpriseMVVMTemplate.DesktopClient.Base;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels;
using EnterpriseMVVMTemplate.Windows;
using Api.Data.Contexts; using Api.Data.Models;

namespace EnterpriseMVVMTemplate.DesktopClient.Views
{
    /// <summary>
    /// Interaction logic for EditUser.xaml
    /// </summary>
    public partial class RolesView
    {

        public RolesView(ViewModelBase baseViewModel)
            : base(baseViewModel)
        {
            InitializeComponent();
        }


        private void RolesGrid_OnMouseDoubleClick_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGrid dataGrid = sender as DataGrid;
            if (dataGrid != null)
            {
                EditRoleViewModel ervm = new EditRoleViewModel(dataGrid.SelectedItem as Role);
                EditRole roleWindow = new EditRole(ervm, Switcher.PageSwitcher);
                roleWindow.ShowDialog();
            }
        }
    }
}
