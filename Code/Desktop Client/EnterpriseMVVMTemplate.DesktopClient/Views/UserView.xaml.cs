﻿using System.Windows;
using EnterpriseMVVMTemplate.Windows;

namespace EnterpriseMVVMTemplate.DesktopClient.Views
{
    /// <summary>
    /// Interaction logic for EditUser.xaml
    /// </summary>
    public partial class UserView
    {
        public UserView(ViewModelBase baseViewModel) : base(baseViewModel)
        {
            InitializeComponent();
        }

        
        private void EditUser_OnLoaded(object sender, RoutedEventArgs e)
        {
            UsernameTextBox.Focus();
        }
    }
}
