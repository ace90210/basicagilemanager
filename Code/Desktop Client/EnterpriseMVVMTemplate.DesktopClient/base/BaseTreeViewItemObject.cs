﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using EnterpriseMVVMTemplate.Windows;

namespace EnterpriseMVVMTemplate.DesktopClient.@base
{
    public class BaseTreeViewItemObject : INotifyPropertyChanged
    {
        private ViewModelBase _item;
        private string _title;
        private bool _isSelected;
        private bool _isExpanded;

        public BaseTreeViewItemObject(string title, ViewModelBase item)
        {
            Title = title;
            _item = item;
        }

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
                OnPropertyChanged("Title");
            }
        }

        /// <summary>
        /// Gets/sets whether the TreeViewItem 
        /// associated with this object is selected.
        /// </summary>
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (value != _isSelected)
                {
                    _isSelected = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }

        /// <summary>
        /// Gets/sets whether the TreeViewItem 
        /// associated with this object is expanded.
        /// </summary>
        public bool IsExpanded
        {
            get { return _isExpanded; }
            set
            {
                if (value != _isExpanded)
                {
                    _isExpanded = value;
                    OnPropertyChanged("IsExpanded");
                }
            }
        }

        public ViewModelBase Item
        {
            get { return _item; }
        }

        public virtual ObservableCollection<BaseTreeViewItemObject> Children
        {
            get
            {
                return null;
            }
        } 
        
        // Declare the event 
        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event 
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
