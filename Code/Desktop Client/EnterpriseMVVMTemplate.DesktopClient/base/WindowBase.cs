﻿using System.Windows.Controls;
using EnterpriseMVVMTemplate.DesktopClient.Views;
using EnterpriseMVVMTemplate.Windows;
using System;
using System.Windows;

namespace EnterpriseMVVMTemplate.DesktopClient.Base
{
    public class WindowBase : Window
    {
        private readonly WindowBase _parentWindow;

        public WindowBase(ViewModelBase model, WindowBase parentWindow = null, bool offlineMode = false)
        {
            OfflineMode = offlineMode;
            if (parentWindow != null)
            {
                _parentWindow = parentWindow;
            }
            DataContext = model;
            model.CloseEvent += Context_CloseEvent;
            Loaded += WindowBase_Loaded;
        }

        void WindowBase_Loaded(object sender, RoutedEventArgs e)
        {
            Loaded -= WindowBase_Loaded;
            if (StaticDataController.LoggedInUser == null && !OfflineMode)
            {
                Login loginWindow = new Login("Error: login details lost.");

                loginWindow.Show();
                if (_parentWindow != null)
                {
                    _parentWindow.Close();
                }
                Close();

                //used for overriding login, 1 = id of myself
                // StaticDataController.LoggedInUser = new BusinessContext().GetUserById(1);
            }
        }

        private void CloseWindow()
        {            
            Focus();
            Close();
        }

        void Context_CloseEvent(object sender, EventArgs e)
        {
            CloseWindow();
        }

        protected void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            CloseWindow();
        }


        public void NavigateTo(UserControl nextPage)
        {
            ContentControl present = (ContentControl)FindName("MainContent");

            if (present != null)
            {
                present.Content = nextPage;
            }
        }

        public void NavigateTo(UserControl nextPage, object data)
        {
            ContentControl present = (ContentControl)FindName("MainContent");

            if (present != null)
            {
                present.Content = nextPage;
                ISwitchable s = nextPage as ISwitchable;

                if (s != null)
                    s.UtilizeData(data);
                else
                    throw new ArgumentException("NextPage is not ISwitchable! "
                                                + nextPage.Name);
            }
        }

        public bool OfflineMode { get; set; }
    }
    
    //page switcher
    public static class Switcher
    {
        public static WindowBase PageSwitcher;

        public static void Switch(UserControl newView)
        {
            PageSwitcher.NavigateTo(newView);
        }


        public static void Switch(UserControl newView, object data)
        {
            PageSwitcher.NavigateTo(newView, data);
        }
    }
}
