﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace EnterpriseMVVMTemplate.Windows
{
    public static class DataSecurityManager
    {
        private const string Key = @"/@dRvTTE0a(2$£+9";

        #region Hash

        /// <summary>
        /// Creates a hash using SHA512 and self generated salt, uses input string and additional static data combined with security key to create a unique salt.
        /// Note: any change to additional data will result i a different hash result so be sure data will never change in future
        /// </summary>
        /// <param name="inputString">The string you wish to hash</param>
        /// <param name="cypher">which cypher to us with salt.</param>
        /// <param name="salt">Uniquely generated salt for password. if empty new secure salt generated</param>
        /// <param name="additionalData">optional: additional data to use a when generating a salt.</param>
        /// <returns></returns>
        public static string HashString(SecureString inputString, Cypher cypher, ref byte[] salt, params string[] additionalData)
        {
            string passWithSalt;

            if (inputString == null || inputString.Length == 0)
            {
                throw new ArgumentException("The string to be hashed must exist and have a value");
            }

            if (additionalData != null)
            {
                passWithSalt = GenerateSalt(ConvertFromSecureString(inputString), cypher, 64, additionalData);
            }
            else
            {
                passWithSalt = GenerateSalt(ConvertFromSecureString(inputString), cypher, 64);
            }

            if (salt == null || salt.Length == 0)
            {
                //generate new salt
                salt = GenSecureSalt(64);
            }
            passWithSalt = passWithSalt + Encoding.Default.GetString(salt);

            //add secure salt with password
            UnicodeEncoding ue = new UnicodeEncoding();
            byte[] hashValue;
            byte[] message = ue.GetBytes(passWithSalt);

            SHA512Managed hashString = new SHA512Managed();
            string hex = "";

            hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue)
            {
                hex += String.Format("{0:x2}", x);
            }

            return hex;
        }

        //generates a seeded salt from the input stringw
        private static string GenerateSalt(string inputString, Cypher cypher, int sizeOfSalt, params string[] additionalData)
        {
            Dictionary<char, char> cypherDictionary = CharecterMappingLibrary.GetMappingDictionary(cypher);

            //do not allow empty input string
            if (string.IsNullOrEmpty(inputString))
            {
                throw new ArgumentException("The string to be hashed must exist and have a value");
            }
            StringBuilder mergedString = new StringBuilder();
            int index = 0, keyIndex = 0, mergeIndex = 0;

            //merge additional data with input string
            foreach (string data in additionalData)
            {
                foreach (char c in data)
                {
                    char origChar = inputString[index % inputString.Length];
                    char mergeChar = c;
                    char cypherMergeChar = cypherDictionary[mergeChar];
                    char cypherOrigChar = cypherDictionary[origChar];

                    mergedString.Append(cypherMergeChar);
                    mergedString.Append(cypherOrigChar);

                    index++;
                    mergeIndex++;
                }
                mergeIndex = 0;
            }

            //loop through each charector and use cypher mixed with _key
            StringBuilder outputString = new StringBuilder();
            index = 0;
            mergeIndex = 0;
            while (outputString.Length < sizeOfSalt)
            {
                char origChar = inputString[index % inputString.Length];
                char keyChar = Key[keyIndex % Key.Length];
                char cypherOrigChar = cypherDictionary[origChar];

                outputString.Append(keyChar);
                outputString.Append(cypherOrigChar);

                index++;
                keyIndex++;

                //if exists mix in merge string
                if(mergedString.Length > 0)
                {
                    char mergeChar = mergedString[mergeIndex % mergedString.Length];
                    outputString.Append(mergeChar);
                    mergeIndex++;
                }
            }
            return outputString.ToString();
        }


        private static byte[] GenSecureSalt(int sizeInBytes)
        {
            byte[] output = new byte[sizeInBytes];

            RNGCryptoServiceProvider saltGen = new RNGCryptoServiceProvider();
            saltGen.GetBytes(output);
            return output;
        }

        private static string ConvertFromSecureString(SecureString input)
        {
            IntPtr valuePtr = IntPtr.Zero;
            try
            {
                valuePtr = Marshal.SecureStringToGlobalAllocUnicode(input);
                return Marshal.PtrToStringUni(valuePtr);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(valuePtr);
            }
        }
        #endregion

        #region Encryption

        public static string EncryptString(string input, ref byte[] salt)
        {
            if (salt == null || salt.Length == 0)
            {
                //generate new salt
                salt = GenSecureSalt(64);
            }
            return EncryptString(input, Encoding.Default.GetString(salt));
        }

        public static string EncryptString(string input, string password)
        {
            // Get the bytes of the string
            byte[] bytesToBeEncrypted = Encoding.UTF8.GetBytes(input);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

            // Hash the password with SHA256
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesEncrypted = AES_Encrypt(bytesToBeEncrypted, passwordBytes);

            string result = Convert.ToBase64String(bytesEncrypted);

            return result;
        }

        private static byte[] AES_Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged aes = new RijndaelManaged())
                {
                    aes.KeySize = 256;
                    aes.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    aes.Key = key.GetBytes(aes.KeySize / 8);
                    aes.IV = key.GetBytes(aes.BlockSize / 8);

                    aes.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }
                    encryptedBytes = ms.ToArray();
                }
            }

            return encryptedBytes;
        }
        #endregion

        #region Decyption

        public static string DecryptString(string input, byte[] salt)
        {
            if(salt == null || salt.Length == 0)
            {
                return null;
            }
            return DecryptString(input, Encoding.Default.GetString(salt));
        }

        public static string DecryptString(string input, string password)
        {
            string result;
            try
            {
                // Get the bytes of the string
                byte[] bytesToBeDecrypted = Convert.FromBase64String(input);
                byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
                passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

                byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes);

                result = Encoding.UTF8.GetString(bytesDecrypted);
            }
            catch (Exception)
            {
                //anything goes wrong return original input
                return input;
            }
            return result;
        }

        private static byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged aes = new RijndaelManaged())
                {
                    aes.KeySize = 256;
                    aes.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    aes.Key = key.GetBytes(aes.KeySize / 8);
                    aes.IV = key.GetBytes(aes.BlockSize / 8);

                    aes.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }

        #endregion

        #region Cypher
        public enum Cypher
        {
            Default, Alternative
        }

        private static class CharecterMappingLibrary
        {
            public static string GetCompatibleRegex(Cypher cypher = Cypher.Default, int minLength = 0, int maxLength = 128)
            {
                switch (cypher)
                {
                        case Cypher.Alternative:
                            return "^[a-z0-9_-]{" + minLength + "," + maxLength + "}$";
                        default:
                            return "^[a-zA-Z0-9_-]{" + minLength + "," + maxLength + "}$";
                }
            }

            public static Dictionary<char, char> GetMappingDictionary(Cypher cypher = Cypher.Default)
            {
                //DEFAULT CYPHER, ALT not implemented yet
                Dictionary<char, char> defaultMappings = new Dictionary<char, char>
                {
                    {'a', '}'},
                    {'b', '@'},
                    {'c', '#'},
                    {'d', '"'},
                    {'e', '\\'},
                    {'f', '?'},
                    {'g', '1'},
                    {'h', '>'},
                    {'i', '0'},
                    {'j', '{'},
                    {'k', '['},
                    {'l', '~'},
                    {'m', '^'},
                    {'n', '£'},
                    {'o', '_'},
                    {'p', '<'},
                    {'q', '}'},
                    {'r', '@'},
                    {'s', '#'},
                    {'t', '"'},
                    {'u', '\\'},
                    {'v', '?'},
                    {'w', '1'},
                    {'x', '>'},
                    {'y', '0'},
                    {'z', '{'},
                    {'A', '!'},
                    {'B', '-'},
                    {'C', '}'},
                    {'D', '+'},
                    {'E', '/'},
                    {'F', ':'},
                    {'G', '.'},
                    {'H', 'l'},
                    {'I', '%'},
                    {'J', 'a'},
                    {'K', '~'},
                    {'L', '£'},
                    {'M', 'H'},
                    {'N', '3'},
                    {'O', '('},
                    {'P', '8'},
                    {'Q', '<'},
                    {'R', '@'},
                    {'S', '\\'},
                    {'T', '^'},
                    {'U', '['},
                    {'V', '!'},
                    {'W', '1'},
                    {'X', ';'},
                    {'Y', 'p'},
                    {'Z', ' '},
                    {'0', '%'},
                    {'1', 'a'},
                    {'2', '~'},
                    {'3', '£'},
                    {'4', '^'},
                    {'5', '£'},
                    {'6', '_'},
                    {'7', '<'},
                    {'8', '}'},
                    {'9', '@'},
                    {'-', 'f'},
                    {'_', 'K'},
                    {' ', '*'}
                };

                return defaultMappings;
            }
        }
        #endregion
    }
}
