﻿using Api.Data.Contexts; using Api.Data.Models;

namespace EnterpriseMVVMTemplate.Windows
{
    public static class StaticDataController
    {
        public static User LoggedInUser { get; set; }

        private static ConnectionTypeList _connectionType;

        public static ConnectionTypeList ConnectionType
        {
            get { return _connectionType; }
            set
            {
                _connectionType = value;
                BusinessContext.ConnectionType = _connectionType;
            }
        }

        private static ConnectionDatabaseList _connectionDatabase;

        public static ConnectionDatabaseList ConnectionDatabase
        {
            get { return _connectionDatabase; }
            set
            {
                _connectionDatabase = value;
                BusinessContext.ConnectionDatabase = _connectionDatabase;
            }
        }
    }
}
