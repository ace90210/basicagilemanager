﻿using System;
using System.Collections.Generic;

namespace EnterpriseMVVMTemplate.Windows.ValidationRules
{
    public class ValidationRule
    {
        #region Constructors
        public ValidationRule(string friendlyName, string propertyName)
        {
            FriendlyName = friendlyName;
            PropertyName = propertyName;


            //check mandatory data is valid
            if (string.IsNullOrWhiteSpace(FriendlyName))
            {
                throw new ArgumentException("You must provide a friendly name for the property to be validated");
            }

            if (string.IsNullOrWhiteSpace(PropertyName))
            {
                throw new ArgumentException("You must provide a property name to be validated");
            }
        }

        public ValidationRule(string friendlyName, string propertyName, CustomValidationRule customValidation) : this(friendlyName, propertyName)
        {
            UseCustomDelegate = true;
            CustomValidation = customValidation;
            
            if (customValidation == null)
            {
                throw new ArgumentException("You must provide a valid custom validation delegate");
            }
        }
        #endregion

        #region Properties

        public delegate string CustomValidationRule();

        public CustomValidationRule CustomValidation;

        public string FriendlyName { get; private set; }
        public string PropertyName { get; private set; }
        public bool UseCustomDelegate { get; set; }

        private bool _useCustomDelegateOnly;

        public bool UseCustomDelegateOnly
        {
            get { return _useCustomDelegateOnly; }
            set
            {
                if (value)
                {
                    UseCustomDelegate = true;
                }
                _useCustomDelegateOnly = value;
            }
        }

        #endregion

        #region Methods
        public virtual string Validate(object propertyValue, ref List<string> errors)
        {
            string error = null;

            if (UseCustomDelegate && CustomValidation != null)
            {
                error = CustomValidation();
            }

            return error;
        }
        #endregion
    }
}
