using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace EnterpriseMVVMTemplate.Windows.ValidationRules
{

    public enum CaseRuleType
    {
        UpperCaseOnly,
        LowerCaseOnly,
        NoMixedCases,
        None
    }

    public class ValidationStringRule : ValidationRule
    {
        #region Constructors
        public ValidationStringRule(string friendlyName, string propertyName, string regexToMatch = null, List<string> expliciteValidValues = null,
            List<string> explicitInvalidValues = null, bool allowWhitespace = false)
            : base(friendlyName, propertyName)
        {
            AllowWhitespace = allowWhitespace;
            RegexToMatch = regexToMatch;
            ValidValues = expliciteValidValues;
            InvalidValues = explicitInvalidValues;
            ExactValidInvalidOnly = true;
            CaseRule = CaseRuleType.None;
        }


        public ValidationStringRule(string friendlyName, string propertyName, CustomValidationRule customValidation, bool allowWhitespace) 
            : base(friendlyName, propertyName, customValidation)
        {
            AllowWhitespace = allowWhitespace;
            ExactValidInvalidOnly = true;
            CaseRule = CaseRuleType.None;
        }
        #endregion

        #region Properties
        public bool AllowWhitespace { get; set; }
        public string RegexToMatch { get; set; }
        public List<string> ValidValues { get; set; }
        public List<string> InvalidValues { get; set; }
        public bool ExactValidInvalidOnly { get; set; }
        public int? MinLength { get; set; }
        public int? MaxLength { get; set; }
        public CaseRuleType CaseRule { get; set; }

        //error message overrides
        public string OverrideInvalidValuesMessage { get; set; }
        #endregion

        #region Methods

        public override string Validate(object propertyValue, ref List<string> errors)
        {
            int? minLength = MinLength;
            int? maxLength = MaxLength;

            //error string list
            string invalidPropertyType = string.Format("{0} is an invalid type", FriendlyName);
            string noUserEntered = string.Format("A {0} is must be entered", FriendlyName);
            string upperOnly = string.Format("{0} must be upper case only", FriendlyName);
            string lowerOnly = string.Format("{0} must be lower case only", FriendlyName);
            string notMixedCase = string.Format("{0} must not mix upper and lower cases", FriendlyName);
            string regex = string.Format("A valid {0} must be entered", FriendlyName);
            string validValueList = string.Format("{0} must be an allowed value", FriendlyName);
            string invalidValueList = OverrideInvalidValuesMessage ?? string.Format("{0} is a disallowed value", FriendlyName);

            string stringToShort = String.Empty;
            if (minLength != null)
            {
                stringToShort = string.Format("{0} must be at least {1} charectors long.", FriendlyName, minLength);
            }

            string stringToLong = String.Empty;
            if (maxLength != null)
            {
                stringToLong = string.Format("{0} must be at most {1} charectors long.", FriendlyName, maxLength);
            }

            //clear any existing errors
            errors.Remove(invalidPropertyType);
            errors.Remove(noUserEntered);
            errors.Remove(upperOnly);
            errors.Remove(lowerOnly);
            errors.Remove(notMixedCase);
            errors.Remove(regex);
            errors.Remove(validValueList);
            errors.Remove(invalidValueList);
            errors.Remove(stringToShort);
            errors.Remove(stringToLong);


            if (!(propertyValue is string))
            {
                errors.Add(invalidPropertyType);
                return invalidPropertyType;
            }
            string value = propertyValue as string;

            string error = base.Validate(value, ref errors);

            if (UseCustomDelegateOnly)
            {
                return error;
            }

            //check not empty
            if (AllowWhitespace)
            {
                if (string.IsNullOrEmpty(value))
                {
                    errors.Add(noUserEntered);
                    error = noUserEntered;
                }
            }
            else
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    errors.Add(noUserEntered);
                    error = noUserEntered;
                }
            }


            //check lengths of string
            if (minLength != null && (string.IsNullOrEmpty(value) || value.Length < minLength))
            {
                errors.Add(stringToShort);
                error = stringToShort;
            }

            if (maxLength != null && !string.IsNullOrEmpty(value) && value.Length > maxLength)
            {

                errors.Add(stringToLong);
                error = stringToLong;
            }

            //check case rules
            switch (CaseRule)
            {
                case CaseRuleType.UpperCaseOnly:
                    {
                        if (!string.IsNullOrEmpty(value) && !value.Equals(value.ToUpper()))
                        {
                            errors.Add(upperOnly);
                            error = upperOnly;
                        }
                    } break;
                case CaseRuleType.LowerCaseOnly:
                    {
                        if (!string.IsNullOrEmpty(value) && !value.Equals(value.ToLower()))
                        {
                            errors.Add(lowerOnly);
                            error = lowerOnly;
                        }
                    } break;
                case CaseRuleType.NoMixedCases:
                    {
                        if (!string.IsNullOrEmpty(value) &&
                            (!value.Equals(value.ToUpper()) && !value.Equals(value.ToLower())))
                        {
                            errors.Add(notMixedCase);
                            error = notMixedCase;
                        }
                    } break;
            }

            //check regex rule
            if (RegexToMatch != null)
            {
                if (string.IsNullOrWhiteSpace(value) ||
                    !Regex.Match(value, RegexToMatch).Success)
                {
                    errors.Add(regex);
                    error = regex;
                }
            }

            //check valid values
            if (ValidValues != null && ValidValues.Count > 0 && !string.IsNullOrWhiteSpace(value))
            {
                if (
                        (ExactValidInvalidOnly && !ValidValues.Any(vv => vv.Equals(value))) ||
                        (!ExactValidInvalidOnly && !ValidValues.Any(vv => vv.Contains(value)))
                    )
                {
                    errors.Add(validValueList);
                    error = validValueList;
                }
            }

            //check invalid values
            if (InvalidValues != null && InvalidValues.Count > 0 && !string.IsNullOrWhiteSpace(value))
            {
                if (

                        (ExactValidInvalidOnly && InvalidValues.Any(vv => vv.Equals(value))) ||
                        (!ExactValidInvalidOnly && InvalidValues.Any(vv => vv.Contains(value)))
                    )
                {
                    errors.Add(invalidValueList);
                    error = invalidValueList;
                }
            }
            return error;
        }

        #endregion
    }
}