﻿using EnterpriseMVVMTemplate.Windows.ValidationRules;
using Api.Data.Contexts; using Api.Data.Models;

namespace EnterpriseMVVMTemplate.Windows
{
    using Microsoft.Practices.Unity;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    
    public abstract class ViewModelBase : ObservableObject, IDataErrorInfo
    {
        #region Fields
        protected IBusinessContext Manager;
        protected List<string> Errors = new List<string>();

        protected readonly List<ValidationRule> ValidationRules;
        public static IUnityContainer Container;
        #endregion

        // Declare the event 
        public event EventHandler CloseEvent;

        protected User LoggedInUser;

        protected ViewModelBase()
        {
            ValidationRules = new List<ValidationRule>();
            LoggedInUser = StaticDataController.LoggedInUser;
            Manager = Container.Resolve<IBusinessContext>();
        }

        public string Error
        {
            get { return null; }
        }

        protected List<string> ValidatedProperties = new List<string>();

        public bool HasErrors
        { 
            get
            { return ValidatedProperties.Any(property => this[property] != null); }
        }

        public string this[string columnName]
        {
            get
            {
                string error =  OnValidate(columnName);
                return error;
            }
        }

        protected virtual string OnValidate(string propertyName)
        {
            string error = null;

            //validate rules list
            if (ValidationRules.Any(svr => svr.PropertyName == propertyName))
            {
                ValidationRule rule =
                    ValidationRules.FirstOrDefault(svr => svr.PropertyName == propertyName);

                if (rule != null)
                {
                    object propertyValue = GetType().GetProperty(propertyName).GetValue(this, null);
                    error = rule.Validate(propertyValue, ref Errors);
                }
            }

            if (error != null)
            {
                return error;
            }

            //base validation checks
            var context = new ValidationContext(this)
            {
                    MemberName = propertyName
            };

            var results = new Collection<ValidationResult>();
            bool isValid = Validator.TryValidateObject(this, context, results, true);

            if (isValid)
            {
                return null;
            }
            ValidationResult result = results.SingleOrDefault(p => 
                p.MemberNames.Any(memberName => memberName == propertyName));

            error = result == null ? null : result.ErrorMessage;
            return error;
        }

        public virtual void Refresh(bool renewContext)
        {
            if(renewContext)
                Manager = Container.Resolve<BusinessContext>();
        }

        protected void Close(EventArgs e)
        {
            if(CloseEvent != null)
            {
                CloseEvent.Invoke(this, e);
            }
        }
    }   
}
