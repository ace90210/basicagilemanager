﻿using System.Data.Entity.Core.EntityClient;

namespace EntrepriseMVVMTemplate.Data
{
    public partial class BasicAgileManagerContainer
    {
        public BasicAgileManagerContainer(EntityConnection connection)
            : base(connection, true)
        {
        }
    }
}
