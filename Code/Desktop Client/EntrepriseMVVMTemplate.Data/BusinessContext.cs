﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;

namespace EntrepriseMVVMTemplate.Data
{
    public enum ConnectionTypeList { Local, Remote };
    public enum ConnectionDatabaseList { Live, Development };

    public sealed class BusinessContext : IBusinessContext
    {
        #region Private FIELDS

        private const string ProviderName = "System.Data.SqlClient";
        private const string User = "BasicAgileManager";
        private const string Pass = "IMF04GTace";
        private const string Db = "BasicAgileManager";
        private const string DevDb = "BasicAgileManager_Dev";
/*
        private const string Testdb = "BasicAgileManager_Test";
*/
        private const string RemoteDomain = "www.runeclawgames.com";
        private const string LocalDomain = "HOMESERVER\\HOMESERVERSQL";

        private BasicAgileManagerContainer _context;
        private bool _disposed;

        public static ConnectionTypeList ConnectionType = ConnectionTypeList.Local;
        public static ConnectionDatabaseList ConnectionDatabase = ConnectionDatabaseList.Live;

        #endregion

        #region Constructors
        public BusinessContext()
        {
            //check for existing connection type being set
            if (_context == null)
            {
                string meta;
                SqlConnectionStringBuilder sqlBuilder = new SqlConnectionStringBuilder();

                switch (ConnectionType)
                {
                    case ConnectionTypeList.Remote:
                    {
                        sqlBuilder.DataSource = RemoteDomain;
                    }
                        break;
                    default:
                    {
                        sqlBuilder.DataSource = LocalDomain;
                    }
                        break;
                }
                switch (ConnectionDatabase)
                {
                    case ConnectionDatabaseList.Live:
                        sqlBuilder.InitialCatalog = Db;
                        break;
                    case ConnectionDatabaseList.Development:
                        sqlBuilder.InitialCatalog = DevDb;
                        break;
                }
                meta = string.Format(@"res://*/{0}.csdl|res://*/{0}.ssdl|res://*/{0}.msl", Db);
                

                sqlBuilder.PersistSecurityInfo = true;
                sqlBuilder.UserID = User;
                sqlBuilder.Password = Pass;

                string providerConnectionString = sqlBuilder.ToString();

                EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder();

                entityBuilder.Provider = ProviderName;
                entityBuilder.ProviderConnectionString = providerConnectionString;
                entityBuilder.Metadata = meta;

                EntityConnection connection = new EntityConnection(entityBuilder.ToString());
                _context = new BasicAgileManagerContainer(connection);
            }
        }

        //public BusinessContext(bool testing = false, ConnectionList type = ConnectionList.Local)
        //{
        //    //check for existing connection type being set
        //    if (ConnectionType != null)
        //    {
        //        type = ConnectionType.Value;
        //    }
        //    else
        //    {
        //        ConnectionType = type;
        //    }
        //    if (_context == null)
        //    {
        //        string meta;
        //        SqlConnectionStringBuilder sqlBuilder = new SqlConnectionStringBuilder();
        //        if (testing || IsTesting)
        //        {
        //            IsTesting = true;
        //            switch (type)
        //            {
        //                case ConnectionList.Remote:
        //                    {
        //                        sqlBuilder.DataSource = RemoteDomain;
        //                    } break;
        //                default:
        //                    {
        //                        sqlBuilder.DataSource = LocalDomain;
        //                    } break;
        //            }
        //            sqlBuilder.InitialCatalog = Testdb;
        //            meta = string.Format(@"res://*/{0}.csdl|res://*/{0}.ssdl|res://*/{0}.msl", Testdb);
        //        }
        //        else
        //        {
        //            switch (type)
        //            {
        //                case ConnectionList.Remote:
        //                    {
        //                        sqlBuilder.DataSource = RemoteDomain;
        //                    } break;
        //                default:
        //                    {
        //                        sqlBuilder.DataSource = LocalDomain;
        //                    } break;
        //            }
        //            sqlBuilder.InitialCatalog = Db;
        //            meta = string.Format(@"res://*/{0}.csdl|res://*/{0}.ssdl|res://*/{0}.msl", Db);
        //        }

        //        sqlBuilder.PersistSecurityInfo = true;
        //        sqlBuilder.UserID = User;
        //        sqlBuilder.Password = Pass;

        //        string providerConnectionString = sqlBuilder.ToString();

        //        EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder();

        //        entityBuilder.Provider = ProviderName;
        //        entityBuilder.ProviderConnectionString = providerConnectionString;
        //        entityBuilder.Metadata = meta;

        //        EntityConnection connection = new EntityConnection(entityBuilder.ToString());
        //        _context = new BasicAgileManagerContainer(connection);
        //    }
        //}

        #endregion

        #region Users
        public User AddUser(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user", "user must not be null");
            }

            if (string.IsNullOrWhiteSpace(user.Username) || user.Username.Length < 4)
            {
                throw new ArgumentException("Username must be at least 4 charectors long");
            }

            _context.Users.Add(user);
            SaveChanges();

            return user;
        }

        public User UpdateUser(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user", "user must not be null");
            }
            if (!_context.Users.Any(u => u.Id == user.Id))
            {
                return null;
            }

            var result = from u in _context.Users
                         where u.Id == user.Id
                         select u;

            User updatedUser = result.SingleOrDefault();
            if (updatedUser != null)
            {
                updatedUser.Title = user.Title;
                updatedUser.AddressLineOne = user.AddressLineOne;
                updatedUser.AddressLineTwo = user.AddressLineTwo;
                updatedUser.Town = user.Town;
                updatedUser.Postcode = user.Postcode;
                updatedUser.Telephone = user.Telephone;
                updatedUser.Mobile = user.Mobile;
                updatedUser.Email = user.Email;
                updatedUser.Description = user.Description;
                updatedUser.RoleId = user.RoleId;
                updatedUser.Password = user.Password;
                updatedUser.SecureSalt = user.SecureSalt;
                updatedUser.LastUpdated = user.LastUpdated;
                updatedUser.LastUpdatedById = user.LastUpdatedById;

                SaveChanges();
                return updatedUser;
            }
            return null;
        }

        public User GetUserById(int id)
        {
            return _context.Users.FirstOrDefault(u => u.Id == id);
        }

        public List<User> GetAllUsers()
        {
            var results = from u in _context.Users
                          select u;
            return results.ToList();
        }

        #endregion

        #region Projects

        public Project AddProject(Project project)
        {
            if (project == null)
            {
                throw new ArgumentNullException("project", "Project must not be null");
            }

            if (string.IsNullOrWhiteSpace(project.Title) || project.Title.Length < 4)
            {
                throw new ArgumentException("Project name must not be empty");
            }

            _context.Projects.Add(project);
            SaveChanges();

            return project;
        }

        public Project UpdateProject(Project project)
        {
            if (project == null)
            {
                throw new ArgumentNullException("project", "Project must not be null");
            }
            if (!_context.Projects.Any(u => u.Id == project.Id))
            {
                return null;
            }

            var result = from u in _context.Projects
                         where u.Id == project.Id
                         select u;

            Project updatedProject = result.SingleOrDefault();
            if (updatedProject != null)
            {
                updatedProject.Title = project.Title;
                updatedProject.Description = project.Description;
                updatedProject.LastUpdated = project.LastUpdated;
                updatedProject.LastUpdatedById = project.LastUpdatedById;

                SaveChanges();
                return updatedProject;
            }
            return null;
        }

        public Project GetProjectById(int id)
        {
            return _context.Projects.FirstOrDefault(p => p.Id == id);
        }

        public IEnumerable<Project> GetAllProjects()
        {
            return _context.Projects.ToList();
        }

        #endregion

        #region Releases

        public Release AddRelease(Release release)
        {
            if (release == null)
            {
                throw new ArgumentNullException("release", "Release must not be null");
            }

            if (string.IsNullOrWhiteSpace(release.Title) || release.Title.Length == 0)
            {
                throw new ArgumentException("Release title must not be empty");
            }

            _context.Releases.Add(release);
            SaveChanges();

            return release;
        }

        public Release UpdateRelease(Release release)
        {
            if (release == null)
            {
                throw new ArgumentNullException("release", "Release must not be null");
            }
            if (!_context.Releases.Any(u => u.Id == release.Id))
            {
                return null;
            }

            var result = from u in _context.Releases
                         where u.Id == release.Id
                         select u;

            Release updatedRelease = result.SingleOrDefault();
            if (updatedRelease != null)
            {
                updatedRelease.Title = release.Title;
                updatedRelease.Description = release.Description;
                updatedRelease.LastUpdated = release.LastUpdated;
                updatedRelease.LastUpdatedById = release.LastUpdatedById;

                SaveChanges();
                return updatedRelease;
            }
            return null;
        }

        public Release GetReleaseById(int id)
        {
            return _context.Releases.FirstOrDefault(r => r.Id == id);
        }

        public IEnumerable<Release> GetAllReleases()
        {
            return _context.Releases.ToList();
        }
        #endregion


        #region Iterations

        public Iteration AddIteration(Iteration iteration)
        {
            if (iteration == null)
            {
                throw new ArgumentNullException("iteration", "Iteration must not be null");
            }

            if (string.IsNullOrWhiteSpace(iteration.Title) || iteration.Title.Length == 0)
            {
                throw new ArgumentException("Iteration title must not be empty");
            }

            if (iteration.StartDate == null || iteration.StartDate == DateTime.MinValue)
            {
                throw new ArgumentException("Iteration start date must be valid");
            }

            if (iteration.EndDate == null || iteration.EndDate == DateTime.MinValue)
            {
                throw new ArgumentException("Iteration end date must be valid");
            }

            _context.Iterations.Add(iteration);
            SaveChanges();

            return iteration;
        }

        public Iteration UpdateIteration(Iteration iteration)
        {
            if (iteration == null)
            {
                throw new ArgumentNullException("iteration", "Iteration must not be null");
            }

            if (string.IsNullOrWhiteSpace(iteration.Title) || iteration.Title.Length == 0)
            {
                throw new ArgumentException("Iteration title must not be empty");
            }

            if (iteration.StartDate == null || iteration.StartDate == DateTime.MinValue)
            {
                throw new ArgumentException("Iteration start date must be valid");
            }

            if (iteration.EndDate == null || iteration.EndDate == DateTime.MinValue)
            {
                throw new ArgumentException("Iteration end date must be valid");
            }

            if (!_context.Iterations.Any(u => u.Id == iteration.Id))
            {
                return null;
            }

            var result = from u in _context.Iterations
                         where u.Id == iteration.Id
                         select u;

            Iteration updatedIteration = result.SingleOrDefault();
            if (updatedIteration != null)
            {
                updatedIteration.Title = iteration.Title;
                updatedIteration.StartDate = iteration.StartDate;
                updatedIteration.EndDate = iteration.EndDate;
                updatedIteration.ReleaseId = iteration.ReleaseId;
                updatedIteration.LastUpdated = iteration.LastUpdated;
                updatedIteration.LastUpdatedById = iteration.LastUpdatedById;

                SaveChanges();
                return updatedIteration;
            }
            return null;
        }

        public Iteration GetIterationById(int id)
        {
            return _context.Iterations.FirstOrDefault(i => i.Id == id);
        }

        public IEnumerable<Iteration> GetAllIterations()
        {
            return _context.Iterations.ToList();
        }
        #endregion


        #region Stories

        public Story AddStory(Story story)
        {
           ValidateStory(story);

           _context.Stories.Add(story);
           
          SaveChanges();

            return story;
        }

        public Story UpdateStory(Story story)
        {
           ValidateStory(story);

            if (!_context.Stories.Any(u => u.Id == story.Id))
            {
                return null;
            }

            var result = from u in _context.Stories
                         where u.Id == story.Id
                         select u;

            Story updatedStory = result.SingleOrDefault();
            if (updatedStory != null)
            {
                updatedStory.ReleaseId = story.ReleaseId;
                updatedStory.Title = story.Title;
                updatedStory.Description = story.Description;
                updatedStory.StatusId = story.StatusId;
                updatedStory.OwnerId = story.OwnerId;
                updatedStory.ChangeSet = story.ChangeSet;
                updatedStory.TFSID = story.TFSID;
                updatedStory.Priority = story.Priority;
                updatedStory.IterationId = story.IterationId;
                updatedStory.LastUpdated = story.LastUpdated;
                updatedStory.LastUpdatedById = story.LastUpdatedById;

                SaveChanges();
                return updatedStory;
            }
            return null;
        }

        public Story GetStoryById(int id)
        {
            return _context.Stories.FirstOrDefault(s => s.Id == id);
        }

        public IEnumerable<Story> GetAllStories()
        {
            var q = from s in _context.Stories.Include("Release").Include("Release.Project")
                    select s;
            return q.ToList();
        }

        private void ValidateStory(Story story)
        {
            if (story == null)
            {
                throw new ArgumentNullException("story", "Story must not be null");
            }

            if (string.IsNullOrWhiteSpace(story.Title) || story.Title.Length == 0)
            {
                throw new ArgumentException("Story title must not be empty");
            }

            if (string.IsNullOrWhiteSpace(story.Description) || story.Description.Length == 0)
            {
                throw new ArgumentException("Story description must not be empty");
            }

            if (story.StatusId == 0 || !_context.StoryStatuses.Any(ss => ss.Id == story.StatusId))
            {
                throw new ArgumentException("Story must have a valid status");
            }

            if (story.ReleaseId == 0 || !_context.Releases.Any(r => r.Id == story.ReleaseId))
            {
                throw new ArgumentException("Story must have a valid release id");
            }
        }
        #endregion
        
        #region Story Statuses

        public StoryStatus GetStoryStatusById(int id)
        {
            return _context.StoryStatuses.FirstOrDefault(ss => ss.Id == id);
        }

        public IEnumerable<StoryStatus> GetAllStoryStatuses()
        {
            return _context.StoryStatuses.ToList();
        }
        #endregion

        #region Roles
        public Role AddRole(Role role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role", "role must not be null");
            }

            _context.Roles.Add(role);
            SaveChanges();

            return role;
        }

        public Role UpdateRole(Role role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role", "role must not be null");
            }

            if (!_context.Roles.Any(ro => ro.Id == role.Id))
            {
                return null;
            }

            var result = from r in _context.Roles
                         where r.Id == role.Id
                         select r;

            Role updatedRole = result.SingleOrDefault();
            if (updatedRole != null)
            {
                updatedRole.LastUpdated = role.LastUpdated;
                updatedRole.LastUpdatedById = role.LastUpdatedById;
                updatedRole.RoleTitle = role.RoleTitle;
                updatedRole.Description = role.Description;

                SaveChanges();
                return updatedRole;
            }
            return null;
        }

        public List<Role> GetAllRoles()
        {
            return _context.Roles.ToList();
        }
        #endregion

        private void SaveChanges()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                //try ((System.Data.Entity.Validation.DbEntityValidationException)$exception).EntityValidationErrors
                // in watch list if debug output fails
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                            ve.PropertyName,
                            eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                            ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public BasicAgileManagerContainer DataContext
        {
            get
            {
                return _context;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed || !disposing)
            {
                return;
            }

            if (_context != null)
            {
                _context.Dispose();
            }
            _context = null;
            _disposed = true;
        }

        #endregion
    }
}
