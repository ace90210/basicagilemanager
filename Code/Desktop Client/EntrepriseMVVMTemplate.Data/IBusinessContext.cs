﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntrepriseMVVMTemplate.Data
{
    public interface IBusinessContext: IDisposable
    {
        #region Users
        User AddUser(User user);

        User UpdateUser(User user);

        User GetUserById(int id);

        List<User> GetAllUsers();
        #endregion

        #region Projects
        Project AddProject(Project project);

        Project UpdateProject(Project project);

        Project GetProjectById(int id);

        IEnumerable<Project> GetAllProjects();
        #endregion

        #region Releases
        Release AddRelease(Release release);

        Release UpdateRelease(Release release);

        Release GetReleaseById(int id);

        IEnumerable<Release> GetAllReleases();
        #endregion


        #region Iterations
        Iteration AddIteration(Iteration iteration);

        Iteration UpdateIteration(Iteration iteration);

        Iteration GetIterationById(int id);

        IEnumerable<Iteration> GetAllIterations();
        #endregion

        #region Stories

        Story AddStory(Story story);

        Story UpdateStory(Story story);

        Story GetStoryById(int id);

        IEnumerable<Story> GetAllStories();
        #endregion
        
        #region Story Statuses
        StoryStatus GetStoryStatusById(int id);

        IEnumerable<StoryStatus> GetAllStoryStatuses();
        #endregion

        #region Roles
        Role AddRole(Role role);

        Role UpdateRole(Role role);

        List<Role> GetAllRoles();
        #endregion


        BasicAgileManagerContainer DataContext { get; }

        void Dispose();
    }
}
