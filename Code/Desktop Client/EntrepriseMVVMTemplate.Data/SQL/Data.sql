--drop contraints (users)
ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_Users_CreatedBy]
GO

ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_Users_Roles]
GO

ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_Users_UpdatedBy]
GO

--drop contraints (roles)
ALTER TABLE [dbo].[Roles] DROP CONSTRAINT [FK_Roles_CreatedBy]
GO

ALTER TABLE [dbo].[Roles] DROP CONSTRAINT [FK_Roles_UpdatedBy]
GO

--wipe existing data
delete users
delete roles

--insert initial data
INSERT [dbo].[Roles] ([RoleTitle], [Description], [CreationDate], [CreatedById], [LastUpdatedById], [LastUpdated])
VALUES
( N'Admin', N'Sample Description', current_timestamp, NULL, NULL, current_timestamp)

INSERT [dbo].[Users] ([Name], [AddressLineOne], [AddressLineTwo], [Town], [Postcode], [Telephone], [Mobile], [Email], [Description], [RoleId], [Title], [CreatedById], [CreationDate], [LastUpdatedById], [LastUpdated], [Password], [Username], [UserType]) 
VALUES (N'Barry Wright', N'109 Gipsy Lane', NULL, N'Norwich', N'NR5 8AX', N'01603 464102', N'07903761157', N'wright.barry@gmx.com', N'system designer', 109, N'Mr', NULL, current_timestamp, NULL,current_timestamp, N'2bae9fcbd5ae5b53f2d0dbee3cb4cf74f66df9b602b42c80737c1339bbcffb2dfcd1bcab0faaf571987638e413da2ad2e51651f16383c8b480737eafec0687be', N'ace90210', 'Member')

--update data with new reference ids
--set user ref fields
update Users set RoleId = (select top 1 Id from Roles), CreatedById = (select top 1 Id from Users), LastUpdatedById = (select top 1 Id from users)

--set role ref fields
update Roles set CreatedById = (select top 1 Id from Users), LastUpdatedById = (select top 1 Id from users)

--add contraints back in (users)
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_CreatedBy] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[Users] ([Id]) 
GO 

ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])
GO

ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_UpdatedBy] FOREIGN KEY([LastUpdatedById])
REFERENCES [dbo].[Users] ([Id]) 
GO

--add contraints back in (roles)
ALTER TABLE [dbo].[Roles]  WITH CHECK ADD  CONSTRAINT [FK_Roles_CreatedBy] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[Users] ([Id]) 
GO 

ALTER TABLE [dbo].[Roles]  WITH CHECK ADD  CONSTRAINT [FK_Roles_UpdatedBy] FOREIGN KEY([LastUpdatedById])
REFERENCES [dbo].[Users] ([Id]) 
GO

--check contraints (users)
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_CreatedBy]
GO

ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Roles]
GO

ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_UpdatedBy]
GO

--check contraints (roles)
ALTER TABLE [dbo].[Roles] CHECK CONSTRAINT [FK_Roles_CreatedBy]
GO

ALTER TABLE [dbo].[Roles] CHECK CONSTRAINT [FK_Roles_UpdatedBy]
GO


