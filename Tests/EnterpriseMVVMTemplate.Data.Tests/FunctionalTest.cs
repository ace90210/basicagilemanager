﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using EnterpriseMVVMTemplate.Windows;
using EntrepriseMVVMTemplate.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Moq;
using Microsoft.Practices.Unity;

namespace EnterpriseMVVMTemplate.Data.Tests
{
    [TestClass]
    public class FunctionalTest
    {
        protected static IBusinessContext Context;

        
        [TestInitialize]
        public virtual void TestInitialize()
        {
            ViewModelBase.Container = new UnityContainer();

            ViewModelBase.Container.RegisterType<IBusinessContext, MockBusinessContext>();
            Context = ViewModelBase.Container.Resolve<IBusinessContext>();
        }

        [TestCleanup]
        public virtual void TestCleanup()
        {
            Context.Dispose();
        }
    }
}
