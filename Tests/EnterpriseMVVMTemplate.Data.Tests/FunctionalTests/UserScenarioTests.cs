﻿using System;
using System.Collections.Generic;
using System.Linq;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels;
using EntrepriseMVVMTemplate.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace EnterpriseMVVMTemplate.Data.Tests.FunctionalTests
{
    [TestClass]
    public class UserScenarioTests
    {
        private Mock<IBusinessContext> mock;
        private List<User> store;

        [TestInitialize]
        public void TestInitialize()
        {
            store = new List<User>();

            mock = new Mock<IBusinessContext>();

            mock.Setup(m => m.GetAllUsers()).Returns(store);
            mock.Setup(m => m.AddUser(It.IsAny<User>())).Callback<User>(user => store.Add((user)));
            mock.Setup(m => m.UpdateUser(It.IsAny<User>())).Callback<User>(user =>
                                                                                {
                                                                                    int i = store.IndexOf(user);
                                                                                    store[i] = user;
                                                                                });

        }

    }
}
