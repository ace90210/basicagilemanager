﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using EntrepriseMVVMTemplate.Data;
using Moq;

namespace EnterpriseMVVMTemplate.Data.Tests
{
    public sealed class MockBusinessContext : IBusinessContext
    {
        private bool _disposed;
        #region Setup Data
        IQueryable<Role> roles = new List<Role>
            {
                new Role(){Id = 1, RoleTitle = "Admin", CreationDate = DateTime.Now},
                new Role(){Id = 2, RoleTitle = "Standard User", CreationDate = DateTime.Now, CreatedById = 1},
                new Role(){Id = 3, RoleTitle = "Guest", CreationDate = DateTime.Now, CreatedById = 1}
            }.AsQueryable();

        IQueryable<User> users = new List<User>
            {
                new User(){Username = "ace90210", RoleId = 1, CreationDate = DateTime.Now},
                new User(){Username = "bwright", RoleId = 2, CreationDate = DateTime.Now, CreatedById = 1},
                new User(){Username = "cwright", RoleId = 3, CreationDate = DateTime.Now, CreatedById = 2}
            }.AsQueryable();

        IQueryable<Project> projects = new List<Project>
            {
                new Project(){Title = "Test release 1", OwnerId = 1, CreationDate = DateTime.Now},
                new Project(){Title = "Test release 2", OwnerId = 2, CreationDate = DateTime.Now},
                new Project(){Title = "Sample release 3", OwnerId = 3, CreationDate = DateTime.Now}
            }.AsQueryable();
        
        Mock<DbSet<Project>> mockProjects = new Mock<DbSet<Project>>();
        Mock<DbSet<User>> mockUsers = new Mock<DbSet<User>>();
        Mock<DbSet<Role>> mockRoles = new Mock<DbSet<Role>>();

        #endregion
        private BasicAgileManagerContainer _context;
        Mock<BasicAgileManagerContainer> Mocker = new Mock<BasicAgileManagerContainer>();

        #region Constructors
        public MockBusinessContext()
        {
            mockRoles = new Mock<DbSet<Role>>();
            mockRoles.As<IQueryable<Role>>().Setup(m => m.Provider).Returns(roles.Provider);
            mockRoles.As<IQueryable<Role>>().Setup(m => m.Expression).Returns(roles.Expression);
            mockRoles.As<IQueryable<Role>>().Setup(m => m.ElementType).Returns(roles.ElementType);
            mockRoles.As<IQueryable<Role>>().Setup(m => m.GetEnumerator()).Returns(roles.GetEnumerator());

            mockUsers = new Mock<DbSet<User>>();
            mockUsers.As<IQueryable<User>>().Setup(m => m.Provider).Returns(users.Provider);
            mockUsers.As<IQueryable<User>>().Setup(m => m.Expression).Returns(users.Expression);
            mockUsers.As<IQueryable<User>>().Setup(m => m.ElementType).Returns(users.ElementType);
            mockUsers.As<IQueryable<User>>().Setup(m => m.GetEnumerator()).Returns(users.GetEnumerator());

            mockProjects = new Mock<DbSet<Project>>();
            mockProjects.As<IQueryable<Project>>().Setup(m => m.Provider).Returns(projects.Provider);
            mockProjects.As<IQueryable<Project>>().Setup(m => m.Expression).Returns(projects.Expression);
            mockProjects.As<IQueryable<Project>>().Setup(m => m.ElementType).Returns(projects.ElementType);
            mockProjects.As<IQueryable<Project>>().Setup(m => m.GetEnumerator()).Returns(projects.GetEnumerator());

            _context = Mocker.Object;
        }
        #endregion

        #region Users
        public User AddUser(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user", "user must not be null");
            }

            if (string.IsNullOrWhiteSpace(user.Username) || user.Username.Length < 4)
            {
                throw new ArgumentException("Username must be at least 4 charectors long");
            }

            _context.Users.Add(user);
            SaveChanges();

            return user;
        }

        public User UpdateUser(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user", "user must not be null");
            }
            if (!_context.Users.Any(u => u.Id == user.Id))
            {
                return null;
            }

            var result = from u in _context.Users
                         where u.Id == user.Id
                         select u;

            User updatedUser = result.SingleOrDefault();
            if (updatedUser != null)
            {
                updatedUser.Title = user.Title;
                updatedUser.AddressLineOne = user.AddressLineOne;
                updatedUser.AddressLineTwo = user.AddressLineTwo;
                updatedUser.Town = user.Town;
                updatedUser.Postcode = user.Postcode;
                updatedUser.Telephone = user.Telephone;
                updatedUser.Mobile = user.Mobile;
                updatedUser.Email = user.Email;
                updatedUser.Description = user.Description;
                updatedUser.RoleId = user.RoleId;
                updatedUser.Password = user.Password;
                updatedUser.SecureSalt = user.SecureSalt;
                updatedUser.LastUpdated = user.LastUpdated;
                updatedUser.LastUpdatedById = user.LastUpdatedById;

                SaveChanges();
                return updatedUser;
            }
            return null;
        }

        public User GetUserById(int id)
        {
            return _context.Users.FirstOrDefault(u => u.Id == id);
        }

        public List<User> GetAllUsers()
        {
            Mocker.Setup(m => m.Users).Returns(mockUsers.Object);
            var results = from u in _context.Users
                          select u;
            return results.ToList();
        }

        #endregion

        #region Projects

        public Project AddProject(Project project)
        {
            if (project == null)
            {
                throw new ArgumentNullException("project", "Project must not be null");
            }

            if (string.IsNullOrWhiteSpace(project.Title) || project.Title.Length < 4)
            {
                throw new ArgumentException("Project name must not be empty");
            }

            _context.Projects.Add(project);
            SaveChanges();

            return project;
        }

        public Project UpdateProject(Project project)
        {
            if (project == null)
            {
                throw new ArgumentNullException("project", "Project must not be null");
            }
            if (!_context.Projects.Any(u => u.Id == project.Id))
            {
                return null;
            }

            var result = from u in _context.Projects
                         where u.Id == project.Id
                         select u;

            Project updatedProject = result.SingleOrDefault();
            if (updatedProject != null)
            {
                updatedProject.Title = project.Title;
                updatedProject.Description = project.Description;
                updatedProject.LastUpdated = project.LastUpdated;
                updatedProject.LastUpdatedById = project.LastUpdatedById;

                SaveChanges();
                return updatedProject;
            }
            return null;
        }

        public Project GetProjectById(int id)
        {
            return _context.Projects.FirstOrDefault(p => p.Id == id);
        }

        public IEnumerable<Project> GetAllProjects()
        {
            Mocker.Setup(m => m.Projects).Returns(mockProjects.Object);
            return _context.Projects.ToList();
        }

        #endregion

        #region Releases

        public Release AddRelease(Release release)
        {
            if (release == null)
            {
                throw new ArgumentNullException("release", "Release must not be null");
            }

            if (string.IsNullOrWhiteSpace(release.Title) || release.Title.Length == 0)
            {
                throw new ArgumentException("Release title must not be empty");
            }

            _context.Releases.Add(release);
            SaveChanges();

            return release;
        }

        public Release UpdateRelease(Release release)
        {
            if (release == null)
            {
                throw new ArgumentNullException("release", "Release must not be null");
            }
            if (!_context.Releases.Any(u => u.Id == release.Id))
            {
                return null;
            }

            var result = from u in _context.Releases
                         where u.Id == release.Id
                         select u;

            Release updatedRelease = result.SingleOrDefault();
            if (updatedRelease != null)
            {
                updatedRelease.Title = release.Title;
                updatedRelease.Description = release.Description;
                updatedRelease.LastUpdated = release.LastUpdated;
                updatedRelease.LastUpdatedById = release.LastUpdatedById;

                SaveChanges();
                return updatedRelease;
            }
            return null;
        }

        public Release GetReleaseById(int id)
        {
            return _context.Releases.FirstOrDefault(r => r.Id == id);
        }

        public IEnumerable<Release> GetAllReleases()
        {
            return _context.Releases.ToList();
        }
        #endregion


        #region Iterations

        public Iteration AddIteration(Iteration iteration)
        {
            if (iteration == null)
            {
                throw new ArgumentNullException("iteration", "Iteration must not be null");
            }

            if (string.IsNullOrWhiteSpace(iteration.Title) || iteration.Title.Length == 0)
            {
                throw new ArgumentException("Iteration title must not be empty");
            }

            if (iteration.StartDate == null || iteration.StartDate == DateTime.MinValue)
            {
                throw new ArgumentException("Iteration start date must be valid");
            }

            if (iteration.EndDate == null || iteration.EndDate == DateTime.MinValue)
            {
                throw new ArgumentException("Iteration end date must be valid");
            }

            _context.Iterations.Add(iteration);
            SaveChanges();

            return iteration;
        }

        public Iteration UpdateIteration(Iteration iteration)
        {
            if (iteration == null)
            {
                throw new ArgumentNullException("iteration", "Iteration must not be null");
            }

            if (string.IsNullOrWhiteSpace(iteration.Title) || iteration.Title.Length == 0)
            {
                throw new ArgumentException("Iteration title must not be empty");
            }

            if (iteration.StartDate == null || iteration.StartDate == DateTime.MinValue)
            {
                throw new ArgumentException("Iteration start date must be valid");
            }

            if (iteration.EndDate == null || iteration.EndDate == DateTime.MinValue)
            {
                throw new ArgumentException("Iteration end date must be valid");
            }

            if (!_context.Iterations.Any(u => u.Id == iteration.Id))
            {
                return null;
            }

            var result = from u in _context.Iterations
                         where u.Id == iteration.Id
                         select u;

            Iteration updatedIteration = result.SingleOrDefault();
            if (updatedIteration != null)
            {
                updatedIteration.Title = iteration.Title;
                updatedIteration.StartDate = iteration.StartDate;
                updatedIteration.EndDate = iteration.EndDate;
                updatedIteration.ReleaseId = iteration.ReleaseId;
                updatedIteration.LastUpdated = iteration.LastUpdated;
                updatedIteration.LastUpdatedById = iteration.LastUpdatedById;

                SaveChanges();
                return updatedIteration;
            }
            return null;
        }

        public Iteration GetIterationById(int id)
        {
            return _context.Iterations.FirstOrDefault(i => i.Id == id);
        }

        public IEnumerable<Iteration> GetAllIterations()
        {
            return _context.Iterations.ToList();
        }
        #endregion


        #region Stories

        public Story AddStory(Story story)
        {
           ValidateStory(story);

           _context.Stories.Add(story);
           
          SaveChanges();

            return story;
        }

        public Story UpdateStory(Story story)
        {
           ValidateStory(story);

            if (!_context.Stories.Any(u => u.Id == story.Id))
            {
                return null;
            }

            var result = from u in _context.Stories
                         where u.Id == story.Id
                         select u;

            Story updatedStory = result.SingleOrDefault();
            if (updatedStory != null)
            {
                updatedStory.ReleaseId = story.ReleaseId;
                updatedStory.Title = story.Title;
                updatedStory.Description = story.Description;
                updatedStory.StatusId = story.StatusId;
                updatedStory.OwnerId = story.OwnerId;
                updatedStory.ChangeSet = story.ChangeSet;
                updatedStory.TFSID = story.TFSID;
                updatedStory.Priority = story.Priority;
                updatedStory.IterationId = story.IterationId;
                updatedStory.LastUpdated = story.LastUpdated;
                updatedStory.LastUpdatedById = story.LastUpdatedById;

                SaveChanges();
                return updatedStory;
            }
            return null;
        }

        public Story GetStoryById(int id)
        {
            return _context.Stories.FirstOrDefault(s => s.Id == id);
        }

        public IEnumerable<Story> GetAllStories()
        {
            var q = from s in _context.Stories.Include("Release").Include("Release.Project")
                    select s;
            return q.ToList();
        }

        private void ValidateStory(Story story)
        {
            if (story == null)
            {
                throw new ArgumentNullException("story", "Story must not be null");
            }

            if (string.IsNullOrWhiteSpace(story.Title) || story.Title.Length == 0)
            {
                throw new ArgumentException("Story title must not be empty");
            }

            if (string.IsNullOrWhiteSpace(story.Description) || story.Description.Length == 0)
            {
                throw new ArgumentException("Story description must not be empty");
            }

            if (story.StatusId == 0 || !_context.StoryStatuses.Any(ss => ss.Id == story.StatusId))
            {
                throw new ArgumentException("Story must have a valid status");
            }

            if (story.ReleaseId == 0 || !_context.Releases.Any(r => r.Id == story.ReleaseId))
            {
                throw new ArgumentException("Story must have a valid release id");
            }
        }
        #endregion
        
        #region Story Statuses

        public StoryStatus GetStoryStatusById(int id)
        {
            return _context.StoryStatuses.FirstOrDefault(ss => ss.Id == id);
        }

        public IEnumerable<StoryStatus> GetAllStoryStatuses()
        {
            return _context.StoryStatuses.ToList();
        }
        #endregion

        #region Roles
        public Role AddRole(Role role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role", "role must not be null");
            }

            _context.Roles.Add(role);
            SaveChanges();

            return role;
        }

        public Role UpdateRole(Role role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role", "role must not be null");
            }

            if (!_context.Roles.Any(ro => ro.Id == role.Id))
            {
                return null;
            }

            var result = from r in _context.Roles
                         where r.Id == role.Id
                         select r;

            Role updatedRole = result.SingleOrDefault();
            if (updatedRole != null)
            {
                updatedRole.LastUpdated = role.LastUpdated;
                updatedRole.LastUpdatedById = role.LastUpdatedById;
                updatedRole.RoleTitle = role.RoleTitle;
                updatedRole.Description = role.Description;

                SaveChanges();
                return updatedRole;
            }
            return null;
        }

        public List<Role> GetAllRoles()
        {
            Mocker.Setup(m => m.Roles).Returns(mockRoles.Object);
            return _context.Roles.ToList();
        }
        #endregion

        private void SaveChanges()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine("error");
            }
        }

        public BasicAgileManagerContainer DataContext
        {
            get
            {
                return _context;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed || !disposing)
            {
                return;
            }

            if (_context != null)
            {
                _context.Dispose();
            }
            _context = null;
            _disposed = true;
        }

        #endregion
    }
}
