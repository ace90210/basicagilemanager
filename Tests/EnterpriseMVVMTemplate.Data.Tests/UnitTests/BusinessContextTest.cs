﻿using EnterpriseMVVMTemplate.Windows;
using EntrepriseMVVMTemplate.Data;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace EnterpriseMVVMTemplate.Data.Tests.UnitTests
{
    [TestClass]
    public class BusinessContextTest : FunctionalTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddNewUser_ThrowsException_WhenUserIsNull()
        {
            using (IBusinessContext bc = ViewModelBase.Container.Resolve<IBusinessContext>())
            {
                bc.AddUser(null);
            }
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void AddNewUser_ThrowsException_WhenUsernameIsEmpty()
        {
            Role role = Context.GetAllRoles().FirstOrDefault(r => r.RoleTitle.ToLower().Contains("admin"));
            Assert.IsNotNull(role);
                
            User newUser = new User
                        {
                            Title = "Mr",
                            Name = "",
                            Username = "",
                            Password="TestPassword",
                            RoleId = role.Id
                        };
            Context.AddUser(newUser);
        }
    }    
}
