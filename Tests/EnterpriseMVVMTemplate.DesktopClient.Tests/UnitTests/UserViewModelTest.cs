﻿using System;
using System.Collections.Generic;
using System.Linq;
using EnterpriseMVVMTemplate.Data.Tests;
using EnterpriseMVVMTemplate.DesktopClient.ViewModels;
using EnterpriseMVVMTemplate.Windows;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EntrepriseMVVMTemplate.Data;
using Moq;

namespace EnterpriseMVVMTemplate.DesktopClient.Tests.UnitTests
{
    

    [TestClass]
    public class UserViewModelTest : FunctionalTest
    {
        [TestMethod]
        public void IsViewModel()
        {
            Assert.IsTrue(typeof (UserViewModel).BaseType == typeof (ViewModelBase));
        }

        [TestMethod]
        public void ValidationErrorWhenUserNameExceeds32Characters()
        {
            UserViewModel viewModelBase = new UserViewModel(new User{ Name = "asddfsasddfsasddfsasddfsasddfsasddfsasddfsasddfsasddfs"} );
            string name = viewModelBase["Name"];
            Assert.IsNotNull(name);
        }

        [TestMethod]
        public void ValidationErrorWhenUserNameIsNotGreaterThanOrEqualTo4Characters()
        {
            UserViewModel viewModelBase = new UserViewModel(new User { Name = "B" });

            Assert.IsNotNull(viewModelBase["Name"]);
        }
        
        [TestMethod]
        public void ValidationErrorWhenUserNameIsNotProvided()
        {
            UserViewModel viewModelBase = new UserViewModel(new User { Name = null });

            Assert.IsNotNull(viewModelBase["Name"]);
        }
    }    
}
