﻿using System.Collections.Generic;
using System.Security;

namespace EnterpriseMVVMTemplate.Windows.Tests.FunctionalTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Windows;
    
    [TestClass]
    public class DataSecurityManagerTest
    {
        [TestMethod]
        public void PasswordHashProducesRepeatableResult_DefaultSettings()
        {
            string password = "samplepassword123-_";
            SecureString securepassword = new SecureString();
            foreach (char c in password)
            {
                securepassword.AppendChar(c);
            }
            byte[] salt = new byte[0];
            string output = DataSecurityManager.HashString(securepassword, DataSecurityManager.Cypher.Default, ref salt);

            string expectedOutput = DataSecurityManager.HashString(securepassword, DataSecurityManager.Cypher.Default, ref salt);

            Assert.IsTrue(output.Equals(expectedOutput));
        }

        [TestMethod]
        public void EncryptStringCreatesEncryptedString()
        {
            string password = "samplepassword123-_";
            string stringToEncrypt= @"please Encrypt this data that contians - _ %^&!)";

            string expectedOutput = "haibu0mMAyRyhUIkui8lfHP4h2ENmba12FaFK5JXlmOMflKszS3oH/tTEprPNv7laaNdWW28XrVLyvNYrpN8TA==";
            string output = DataSecurityManager.EncryptString(stringToEncrypt, password);

            Assert.IsTrue(output.Equals(expectedOutput));
        }

        [TestMethod]
        public void DecryptStringCreatesOriginalString()
        {
            string password = "samplepassword123-_";
            string stringToDecrypt = "haibu0mMAyRyhUIkui8lfHP4h2ENmba12FaFK5JXlmOMflKszS3oH/tTEprPNv7laaNdWW28XrVLyvNYrpN8TA==";

            string expectedOutput = @"please Encrypt this data that contians - _ %^&!)";
            string output = DataSecurityManager.DecryptString(stringToDecrypt, password);

            Assert.IsTrue(output.Equals(expectedOutput));
        }
        
        [TestMethod]
        public void PasswordHashProducesRepeatableResult_MultipleDataValues()
        {
            string password = "samplepassword123-_";
            SecureString securepassword = new SecureString();
            foreach (char c in password)
            {
                securepassword.AppendChar(c);
            }
            List<string> additionalData = new List<string>();
            
            additionalData.Add("user_name");
            additionalData.Add("123456");
            additionalData.Add("Additional-stuff");
            additionalData.Add("test");
            additionalData.Add("5248 48545-");
            byte[] salt = new byte[0];
            string expectedOutput = DataSecurityManager.HashString(securepassword, DataSecurityManager.Cypher.Default, ref salt, additionalData.ToArray());


            string output = DataSecurityManager.HashString(securepassword, DataSecurityManager.Cypher.Default, ref salt, additionalData.ToArray());

            Assert.IsTrue(output.Equals(expectedOutput));
        }
    }
}
