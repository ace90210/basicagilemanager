﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace EnterpriseMVVMTemplate.Windows.Tests.UnitTests
{
    [TestClass]
    public class ActionCommandTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructorThrowsExceptionIfActionParameterIsNull()
        {
            ActionCommand command = new ActionCommand(null);
        }

        [TestMethod]
        public void ExecuteInvokesAction()
        {
            bool invoked = false;

            Action<object> action = obj => invoked = true;

            ActionCommand command = new ActionCommand(action);

            command.Execute();

            Assert.IsTrue(invoked);
        }

        [TestMethod]
        public void ExecuteOverloadInvokesActionWithParameter()
        {
            bool invoked = false;

            Action<object> action = obj =>
                                    {
                                        Assert.IsNotNull(obj);
                                        invoked = true;
                                    };

            ActionCommand command = new ActionCommand(action);

            command.Execute(new object());

            Assert.IsTrue(invoked);
        }

        [TestMethod]
        public void CanExecuteIsTrueByDefault()
        {
            ActionCommand command = new ActionCommand(obj => { });

            Assert.IsTrue(command.CanExecute(null));
        }

        [TestMethod]
        public void CanExecuteOverloadExecutesTruePredicate()
        {
            ActionCommand command = new ActionCommand(obj => { }, obj => (int)obj == 1);

            Assert.IsTrue(command.CanExecute(1));
        }

        [TestMethod]
        public void CanExecuteOverloadExecutesFalsePredicate()
        {
            ActionCommand command = new ActionCommand(obj => { }, obj => (int)obj == 1);

            Assert.IsFalse(command.CanExecute(0));
        }
    }
}
