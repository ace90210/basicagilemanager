﻿using EnterpriseMVVMTemplate.Data.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EnterpriseMVVMTemplate.Windows.Tests.UnitTests
{

    [TestClass]
    public class ViewModelTests : FunctionalTest
    {
        [TestMethod]
        public void IsAbstractBaseClass()
        {
            Type t = typeof(ViewModelBase);

            Assert.IsTrue(t.IsAbstract);
        }

        [TestMethod]
        public void IsIDataErrorInfo()
        {
            Assert.IsTrue(typeof(IDataErrorInfo).IsAssignableFrom(typeof(ViewModelBase)));
        }

        [TestMethod]
        public void IsObservableObject()
        {
            Assert.IsTrue(typeof(ViewModelBase).BaseType == typeof(ObservableObject));
        }

        [TestMethod]
        public void IndexerPropertyValidatesPropertyNameWithInvalidValue()
        {

            StubViewModelBase viewModelBase = new StubViewModelBase();

            Assert.IsNotNull(viewModelBase["RequiredProperty"]);
        }

        [TestMethod]
        public void IndexerValidatesPropertyNameWithValidValue()
        {
            StubViewModelBase viewModelBase = new StubViewModelBase
            {
                RequiredProperty = "Test Value"
            };

            Assert.IsNull(viewModelBase["RequiredProperty"]);
        }

        [TestMethod]
        public void IndexerReturnsErrorMessageForRequestedInvalidProperty()
        {
            ViewModelBase viewModel = new StubViewModelBase
                                    {
                                        RequiredProperty = null,
                                        SomeOtherProperty = null
                                    };

            string msg = viewModel["SomeOtherProperty"];

            Assert.AreEqual("The SomeOtherProperty field is required.", msg);
        }

        class StubViewModelBase : ViewModelBase
        {
            [Required]
            public string RequiredProperty
            {
                get;
                set;
            }


            [Required]
            public string SomeOtherProperty
            {
                get;
                set;
            }
        }
    }
}
